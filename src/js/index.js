function activarGeo(){
    $(".btn-activo").removeClass('btn-activo').addClass('btn-desact');
    $("#btn-1").removeClass('btn-desact').addClass('btn-activo');
  }
  function activarClima(){
    $(".btn-activo").removeClass('btn-activo').addClass('btn-desact');
    $("#btn-2").addClass('btn-activo').removeClass('btn-desact');
  }
  function activarFlora(){
    $(".btn-activo").removeClass('btn-activo').addClass('btn-desact');
    $("#btn-3").addClass('btn-activo').removeClass('btn-desact');
  }
  function activarFauna(){
    $(".btn-activo").removeClass('btn-activo').addClass('btn-desact');
    $("#btn-4").addClass('btn-activo').removeClass('btn-desact');
  }

  
  //////////////////////////////////////////////////////////////////// MANANTIALES //////////////////////////////////////////////////////////////////
  document.getElementById("Parque_Natural_Manantiales").addEventListener("click",function( event ) {
    window.location.href = '#nav';
    $("#myAlert").attr("hidden",false);
    setTimeout(function() {
      $("#myAlert").attr("hidden",true);
    },2000);
    /* activarGeo();
    $("#prueba").html("");
    let htmlHeader = 
    `
    <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3391.012446414083!2d-69.3761396842399!3d-31.797410721967886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9686ebfb76fc6fbb%3A0x927d0dcaa1badda8!2sParque%20Nacional%20El%20Leoncito!5e0!3m2!1ses!2sar!4v1642775394327!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    `
    let htmlFin = `` 
    $("#prueba").html(htmlHeader+htmlFin);

    $("#prueba").html(htmlHeader+htmlFin);
    $("#btn-1").attr('onclick',"geoManantiales()");
    $("#btn-2").attr('onclick',"climaManantiales()");
    $("#btn-3").attr('onclick',"floraManantiales()");
    $("#btn-4").attr('onclick',"faunaManantiales()");

    
    $("#descripcion1").html("");
    htmlHeader = 
    `
    <p><b>NOMBRE DEL ANP: </b>Área Protegida Manantiales</p> 

<p><b>CATEGORÍA DE PROTECCIÓN:</b>Paisaje Protegido</p>   

<p><b>OBJETIVO DE CREACIÓN:</b>según el Artículo 2° de la Ley de creación </p>		 
<p><b>ARTÍCULO 2º.-</b>La creación del Área Protegida Manantiales, tiene por objeto:</p>		 
<p>a) Preservar las condiciones ambientales de un sector de la cuenca de altura del río San Juan, asegurando la viabilidad funcional de los ecosistemas de alta montaña contenidos.</p>		
<p>b) Proteger la diversidad biológica.</p>		
<p>c) Preservar importantes yacimientos arqueológicos y áreas de gran trascendencia histórica.</p>		
<p>d) Colaborar en el control de la trashumancia ganadera.</p>		
<p>e) Conservar escenarios naturales de gran belleza paisajísti¬ca, incluyendo al Cerro Mercedario.</p>		
<p>f) Fomentar el desarrollo sustentable de la región.</p>		
<p>g) Fomentar las actividades de investigación científico-técnica, deportivas y recreativas, comerciales y productivas, sociales, culturales y turísticas.</p>		
<p>h) Fomentar la participación de las organizaciones no gubernamentales y otras formas de asociación civil en la gestión del área protegida.</p>		
<p>i) Fomentar la instalación, caracterización y funcionamiento de centros de recreación.</p>		
<p>j) Fomentar la localización, caracterización y destino de edificios, instalaciones y construcciones.</p>
<p>k) Fomentar la coordinación de las actividades en el área entre los distintos niveles de Gobierno, nacional, provincial y municipal, los organismos estatales con competencia sectorial, organizaciones científicas, instituciones educativas, organizaciones no gubernamentales, el sector privado productivo y poblaciones locales.</p>
<p>l) Coadyuvar el equilibrio ecológico zonal.</p>
<p>m) Brindar al público un área de solaz y esparcimiento.</p>
<p>n) Promover el conocimiento de las especies exóticas y autóctonas de la flora y fauna, a fin de valorar debidamente los fines conservacionistas.</p>
<p>o) Posibilitar todas aquellas actividades turísticas y recreativas que estén en consonancia con la finalidad del Parque.</p>

<p><b>LEY Y AÑO DE CREACIÓN:</b> 2315-L, año 2021</p>	

<p><b>AUTORIDAD DE APLICACIÓN:</b>Secretaría Estado Ambiente y Desarrollo Sustentable</p>	 

<p><b>SUPERFICIE:</b>261.000 Ha</p>	 

<p><b>CARACTERÍSTICAS PRINCIPALES:</b></p>	
<p>El área de reserva comprende un territorio Cordillerano de los Andes Medio donde se distinguen unidades geomorfológicas positivas como lo es la Cordillera Principal al occidente y la Cordillera Frontal al oriente con cordones de gran altura que llegan hasta los 6000 m.s.n.m. Los sectores más bajos se encuentran hacia el este con una altura de 1.900 m.s.n.m., en las juntas del Rio Blanco y Rio Los Patos. </p>	
<p>Los relieves negativos se encuentran representados por extensos valles como lo es el Valle de Los Patos Sur o el Valle Hermoso, próximos al límite con la República de Chile en el extremo suroeste del área protegida, por nombrar los más representativos. </p>	
<p>Este enorme sector de la Cordillera constituye la cuenca superior del río San Juan donde se encuentran los glaciares de mayor magnitud y se producen las mayores precipitaciones en la provincia, en consecuencia, suministran el aporte más significativo en caudales hídricos. Cabe destacar que la Provincia de San Juan es la tercera del país en cuando a la superficie ocupada por glaciares, con más de 60.000 hectáreas. Se pueden nombrar en el área protegida los glaciares La Ollada, del Caballito, Karpinski, Ostrowski, del Salto Frío, Italia, del Medio, San Juan y el Ventisquero La Ramada. Todos ellos, asociados al Cordón de La Ramada.</p> 
<p>Además, en la región, existen gran cantidad de humedales representados por las vegas de altura y numerosos arroyos provenientes de los deshielos de los glaciares que como en el caso del arroyo de la Laguna Blanca alimentan la laguna del mismo nombre, que luego escurrirá sus aguas en el Río Blanco. Lo mismo ocurre con los arroyos La Ramada, del Camino, Wanda y Chacayes que vierten sus aguas al Río Colorado que juntamente con el Río Blanco y el Río los Patos conforman los afluentes más importantes del Río San Juan.</p>	
<p>Posee una escasa red de caminos todos ellos circunscriptos al sector sureste destacado por las Ruta Provincial 400 que une la ciudad de Barreal y la zona de Manantiales   y coincidiendo con el límite norte la Ruta provincial 402 que une la misma proyecto minero Pachón.</p>	
<p>La infraestructura edilicia es mínima en relación a la superficie de toda el área protegida y está representada por viejos refugios en distinto grado de mantenimiento, casas pertenecientes a Gendarmería Nacional, ambas habitadas regularmente y Casas Amarillas paraje donde se aloja la sede de la Cooperativa Ganadera del Sur.</p>	

    `
    htmlFin = `` 
    $("#descripcion1").html(htmlHeader+htmlFin);

    $("#descripcion2").html("");
    htmlHeader = 
    `
    <div class="descripcion2">
        <p>Coordenadas	•	31° 18' S y 69° 30' O</p>
        <p>Ecorregiones	•	Altos Andes - Monte de Sierras y Bolsones</p> 
        <p>El Parque Nacional ‘El Leoncito’ se encuentra ubicado al Sudoeste de la Provincia de San Juan, en Calingasta, sobre los faldeos occidentales de las Sierras del Tontal (Precordillera Andina). Latitud Sur: 31° 18' - Longitud Oeste: 69° 30'. </p> 
        <p>ACCESOS VIALES</p> 
        <p>La Ruta Provincial 412, que pasa por la localidad de Barreal, permite acceder al Parque desde el Norte. Desde Uspallata (Mendoza), la Ruta Provincial 39 - continuación mendocina de la Ruta 412 - brinda un ingreso desde el Sur. </p>
        <p>Desde la Ruta 412 hacia el Este, a unos 20 Km. al Sur de Barreal, existe un camino de 12 Km. que conduce a la recepción del Parque Nacional.</p>
        <p>DISTANCIAS AL PARQUE NACIONAL EL LEONCITO</p>
        <p>• San Juan (Capital): 245 Km.</p>
        <p>• Barreal: 35 Km. </p>
        <p>• Mendoza (Capital): 200 Km.</p>
        <p>• Uspallata: 90 Km.</p>
        </div>
    
    `
    htmlFin = `` 
    $("#descripcion2").html(htmlHeader+htmlFin); */
    
  /* }); */
  });
  //////////////////////////////////////////////////////////////////// BARREAL BLANCO //////////////////////////////////////////////////////////////////
  document.getElementById("Barreal_Blanco").addEventListener("click",function( event ) {
    window.location.href = '#nav';
    $("#myAlert").attr("hidden",false);
    setTimeout(function() {
      $("#myAlert").attr("hidden",true);
    },2000);
  });
  //////////////////////////////////////////////////////////////////// BARREAL BLANCO //////////////////////////////////////////////////////////////////
  document.getElementById("Barreal_Blanco").addEventListener("click",function( event ) {
    window.location.href = '#nav';
    $("#myAlert").attr("hidden",false);
    setTimeout(function() {
      $("#myAlert").attr("hidden",true);
    },2000);
  });
    //////////////////////////////////////////////////////////////////// BALDE DE LEYES //////////////////////////////////////////////////////////////////
  document.getElementById("Balde_Leyes").addEventListener("click",function( event ) {
    $("#myAlert").attr("hidden",false);
    window.location.href = '#nav';
    setTimeout(function() {
      $("#myAlert").attr("hidden",true);
    },2000);
  });
    //////////////////////////////////////////////////////////////////// CENTRO //////////////////////////////////////////////////////////////////
  document.getElementById("Centro").addEventListener("click",function( event ) {
    window.location.href = '#nav';
    $("#myAlert").attr("hidden",false);
    setTimeout(function() {
      $("#myAlert").attr("hidden",true);
    },2000);
  });

  //////////////////////////////////////////////////////////////////// EL LEONCITO //////////////////////////////////////////////////////////////////
  document.getElementById("El_Leoncito").addEventListener("click",function( event ) {
    window.location.href = '#prueba';
    activarGeo();
    $("#prueba").html("");
    let htmlHeader = 
    `
    <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3391.012446414083!2d-69.3761396842399!3d-31.797410721967886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9686ebfb76fc6fbb%3A0x927d0dcaa1badda8!2sParque%20Nacional%20El%20Leoncito!5e0!3m2!1ses!2sar!4v1642775394327!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    `
    let htmlFin = `` 
    $("#prueba").html(htmlHeader+htmlFin);

    $("#prueba").html(htmlHeader+htmlFin);
    $("#btn-1").attr('onclick',"geoLeoncito()");
    $("#btn-2").attr('onclick',"climaLeoncito()");
    $("#btn-3").attr('onclick',"floraLeoncito()");
    $("#btn-4").attr('onclick',"faunaLeoncito()");

    /* $("#botones").html("");
    htmlHeader = 
    `
    <button id="btn-1" class="btn-activo" onclick="geoLeoncito()"><i class="fa fa-globe"></i></button>
    <button id="btn-2" class="btn-desact" onclick="climaLeoncito()"><i class="fa fa-cloud"></i></button>
    <button id="btn-3" class="btn-desact" onclick="floraLeoncito()"><i class="fa fa-pagelines"></i></button>
    <button id="btn-4" class="btn-desact" onclick="faunaLeoncito()"><i class="fa fa-horse"></i></button>
    
    `
    htmlFin = `` 
    $("#botones").html(htmlHeader+htmlFin); */

    $("#descripcion1").html("");
    htmlHeader = 
    `
    <h3 class="titulo"><b>RÉGIMEN DE PROTECCIÓN: Parque Nacional</h3>
      <div class="descripcion1">
        <p>ADMINISTRACIÓN: Las 89.706 has. del Parque Nacional ‘El Leoncito’ pertenecen al dominio y jurisdicción de la Administración de Parques Nacionales - responsables del manejo y administración del área - por intermedio de la Intendencia de dicho Parque Nacional. </p>
        <p>OBJETIVO DE CREACIÓN: Su principal objetivo es preservar una muestra representativa de la precordillera cuyana, a ciertas especies de fl ora y fauna - consideradas en peligro y exclusivas de la zona - y a su ambiente natural. Además, se protegen sitios históricos, yacimientos paleontológicos y áreas de interés arqueológico, incluyendo a la pureza atmosférica en el entorno del Observatorio Astronómico. 
        </p>
        <p>LEY Y AÑO DE CREACIÓN: Reserva Natural Estricta ‘El Leoncito’; Decreto Nº 46/94. Parque Nacional ‘El Leoncito’; Ley Nacional Nº 25.656; Año 2002 (modificatoria del Decreto Nº 46/94).
        </p>
        <p>SITUACIÓN GEOGRÁFICA EN SAN JUAN: Está ubicado sobre los faldeos occidentales de las Sierras del Tontal (Precordillera), en el departamento de Calingasta, al SO de la Provincia de San Juan. </p>
        <p>SUPERFICIE: 89.706 hectáreas</p>
        <p>CARACTERÍSTICAS PRINCIPALES:</p>
        <p>• Resguarda una muestra en buen estado de las comunidades típicas de la Precordillera Cuyana, y en especial de la ecoregión del Monte, exclusiva de la Argentina.</p>
        <p>• En su interior funcionan el Observatorio ‘Félix Aguilar’ (ex Observatorio Astronómico ‘Dr. Carlos U. Cesco’) y el Complejo Astronómico ‘El Leoncito’ (CASLEO), siendo este último uno de los más importantes del país.</p>
        <p>• Área de Importancia para la Conservación de las Aves (AICA) por Aves Argentinas/AOP, para la organización BirdLife International (Important Bird Areas, IBAs)</p>
        </div>
    
    `
    htmlFin = `` 
    $("#descripcion1").html(htmlHeader+htmlFin);

    $("#descripcion2").html("");
    htmlHeader = 
    `
    <div class="descripcion2">
        <p>Coordenadas	•	31° 18' S y 69° 30' O</p>
        <p>Ecorregiones	•	Altos Andes - Monte de Sierras y Bolsones</p> 
        <p>El Parque Nacional ‘El Leoncito’ se encuentra ubicado al Sudoeste de la Provincia de San Juan, en Calingasta, sobre los faldeos occidentales de las Sierras del Tontal (Precordillera Andina). Latitud Sur: 31° 18' - Longitud Oeste: 69° 30'. </p> 
        <p>ACCESOS VIALES</p> 
        <p>La Ruta Provincial 412, que pasa por la localidad de Barreal, permite acceder al Parque desde el Norte. Desde Uspallata (Mendoza), la Ruta Provincial 39 - continuación mendocina de la Ruta 412 - brinda un ingreso desde el Sur. </p>
        <p>Desde la Ruta 412 hacia el Este, a unos 20 Km. al Sur de Barreal, existe un camino de 12 Km. que conduce a la recepción del Parque Nacional.</p>
        <p>DISTANCIAS AL PARQUE NACIONAL EL LEONCITO</p>
        <p>• San Juan (Capital): 245 Km.</p>
        <p>• Barreal: 35 Km. </p>
        <p>• Mendoza (Capital): 200 Km.</p>
        <p>• Uspallata: 90 Km.</p>
        </div>
    
    `
    htmlFin = `` 
    $("#descripcion2").html(htmlHeader+htmlFin);
    
  });
///////////////////////////////////////////////////////////  GUANACACHE   //////////////////////////////////////////////////////
  document.getElementById("Guanacache").addEventListener("click",function( event ) {
    window.location.href = '#prueba';
    activarGeo();
    $("#prueba").html("");
    let htmlHeader = 
    `
    <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6745.386040398886!2d-67.35134417816096!3d-32.29323039746557!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x76403ff72438379f!2zMzLCsDE3JzM5LjciUyA2N8KwMjAnNTIuNiJX!5e0!3m2!1ses!2sar!4v1642426321612!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    `
    let htmlFin = `` 
    $("#prueba").html(htmlHeader+htmlFin);
    $("#btn-1").attr('onclick',"geoGuanacache()");
    $("#btn-2").attr('onclick',"climaGuanacache()");
    $("#btn-3").attr('onclick',"floraGuanacache()");
    $("#btn-4").attr('onclick',"faunaGuanacache()");
    /* $("#botones").html("");
    htmlHeader = 
    `
    <button id="btn-1" class="btn-activo" onclick="geoGuanacache()"><i class="fa fa-globe"></i></button>
    <button id="btn-2" class="btn-desact" onclick="climaGuanacache()"><i class="fa fa-cloud"></i></button>
    <button id="btn-3" class="btn-desact" onclick="floraGuanacache()"><i class="fa fa-pagelines"></i></button>
    <button id="btn-4" class="btn-desact" onclick="faunaGuanacache()"><i class="fa fa-horse"></i></button>
    
    ` 
    htmlFin = `` 
    $("#botones").html(htmlHeader+htmlFin); */

    $("#descripcion1").html("");
    htmlHeader = 
    `
    <h3 class="titulo"><b>HUMEDAL - GUANACACHE.</h3>
      <div class="descripcion1">
      <p>CATEGORÍA DE PROTECCIÓN: Sitio Ramsar (Humedal de Importancia Mundial).</p>
        <p>NOMBRE DEL ANP: Sitio Ramsar ‘Lagunas De Huanacache, Del Desaguadero Y Del Bebedero’.</p>
        <p>OBJETIVO DE CREACIÓN: Conservar la biodiversidad y usar racionalmente los humedales a través de la acción a nivel provincial, regional, nacional e internacional, a fin de contribuir al logro de un desarrollo sostenible.</p>
        <p>LEY Y AÑO DE CREACIÓN: Declaración Ramsar 1999. Ampliación del Sitio 2007. Ratifi cado por Ley Nacional Nº 23.319.
        </p>
        <p>AUTORIDAD DE APLICACIÓN: Secretaría de Ambiente y Desarrollo Sustentable. Subsecretaría de Conservación y Áreas Protegidas.
        </p>
        <p> UBICACIÓN GEOGRÁFICA EN SAN JUAN: Al Sur del paralelo 32, en los departamentos de Sarmiento y 25 de Mayo. 
        Con límite Oeste, determinado por la Ruta 40, y límite Este, constituido por el partidor de aguas de la Sierra Guayaguas en el límite con San Luis. 
        El límite Sur queda conformado por el límite interprovincial San Juan-Mendoza y el cauce del Río San Juan.</p>
        <p>SUPERFICIE: 248.000 hectáreas (Jurisdicción San Juan)</p>
        <p>CARACTERÍSTICAS PRINCIPALES:</p>
        <p>• El sitio Ramsar de Las Lagunas de Huanacache, del Desaguadero y del Bebedero, es uno de los sistemas lacustres más importantes de Argentina. Este humedal se encuentra enclavado en el área de ensamble de las tres provincias cuyanas mencionadas, constituyéndose en un tejido conector de gran relevancia natural, paisajística e histórica.</p>
        <p>• Su relevancia biológica es innegable, considerando que este sistema de lagunas y bañados se despliega en un medio geográfico desértico, en el que cumplen una función central para el mantenimiento de los procesos ecológicos.</p>
        <p>• Por otro lado, el sistema de lagunas, a pesar de sus exiguas dimensiones respecto a los parámetros históricos, sigue jugando un rol fundamental para el desarrollo de las poblaciones locales, organizadas en torno a un modelo de producción de subsistencia cuya viabilidad depende de los recursos naturales circundantes. 
        Esta relación de dependencia de las poblaciones humanas vinculadas al humedal ha ido modifi cándose a lo largo del tiempo, asumiendo adaptaciones acordes a los cambios del sistema lagunar.</p>
        </div>
    
    `
    htmlFin = `` 
    $("#descripcion1").html(htmlHeader+htmlFin);

    $("#descripcion2").html("");
    htmlHeader = 
    `
    <div class="descripcion2">
        <p>Coordenadas	•	32° 15' S y 67° 32' O</p>
        <p>Ecorregiones	•	Altos Andes - Monte de Sierras y Bolsones</p> 
        <p>El sitio Ramsar en San Juan se encuentra emplazado al Sur del territorio de la provincia. Su área local se reparte entre los departamentos de Sarmiento y 25 de Mayo. Los límites - establecidos en la ficha técnica de Ramsar para el área de jurisdicción sanjuanina - son los siguientes:</p> 
        <p>- Al Oeste, la Ruta Nacional Nº 40. </p> 
        <p>- Al Sur, el límite interprovincial San Juan - Mendoza y el cauce del Río San Juan.</p>
        <p>- Al Norte, el paralelo 32 S</p>
        <p>- - Al Este, las divisorias de aguas de las Sierras de Guayaguas y el límite provincial con San Luis.</p>
        <p>El sitio Ramsar San Juan comienza a sólo dos kilómetros al Sur de la Villa de Media Agua, en Sarmiento, y a unos 17 kilómetros al Sur - Este de Villa Santa Rosa, cabecera del departamento 25 de Mayo.</p>
        <p>ACCESOS</p>
        <p>El Sitio Ramsar Lagunas de Huanacache es accesible desde la ciudad de San Juan por distintas rutas. Uno de los accesos es la Ruta Nacional Nº 40, ben dirección a Mendoza. El Sitio Ramsar comienza a unos 2 Km. al Sur de Villa Media Agua. 
        Desde allí hasta el limite provincial, se pueden apreciar las características de un importante sector productivo que se destaca por la calidad del melón y la uva. 
        Cercano al límite interprovincial, se puede observar el severo proceso de desertifi cación que afecta a gran parte del Sitio Ramsar cuyano, sector que en siglos pasados se encontraba cubierto de aguas.</p>
        <p>El otro acceso importante es la Ruta Nacional Nº 20, que atraviesa al sitio con dirección Sur-Este vinculando a la provincia con el centro del país. Sobre esta ruta se encuentra El Encón, único poblado urbanizado, ubicado dentro del Sitio Ramsar en la jurisdicción de San Juan. 
        Próximo a este pueblo se pueden observar y disfrutar las bellezas naturales de los médanos cercanos y del bañado del Río San Juan, visitado estacionalmente por importantes poblaciones de fl amencos y otras aves migratorias. 
        Siguiendo rumbo Sur-Este previo a la localidad de Las Trancas, se observan los Bañados del Bermejo, cuyo nivel de agua - dependiendo del caudal del Río San Juan o de las lluvias estacionales - llega a veces hasta la ruta. Existen otras formas de llegar, aunque con menor accesibilidad debido a las condiciones precarias de la red de caminos y de las huellas departamentales. Entre ambas rutas nacionales se encuentra la Ruta Provincial Nº 319, la que pasa por la Comunidad Sawa, Punta del Agua, Colonia San Antonio y Media Agua.</p>
        
        </div>
    
    `
    htmlFin = `` 
    $("#descripcion2").html(htmlHeader+htmlFin);


    
  });
/////////////////////////////////////////////////////// RESERVA SAN GUILLERMO ////////////////////////////////////////////////
  document.getElementById("San_Guillermo").addEventListener("click",function( event ) { 
    window.location.href = '#prueba';
    $("#prueba").html("");
  let htmlHeader = 
  `
  <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d890098.440705852!2d-69.3430181219542!3d-29.371840026116722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x969b420fe75eac21%3A0x18f6eae9c8b423f3!2sParque%20Nacional%20San%20Guillermo!5e0!3m2!1ses!2sar!4v1640614166409!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

  `
  let htmlFin = `` 
  $("#prueba").html(htmlHeader+htmlFin);

  $("#btn-1").attr('onclick',"geoSanGuillermo()");
  $("#btn-2").attr('onclick',"climaSanGuillermo()");
  $("#btn-3").attr('onclick',"floraSanGuillermo()");
  $("#btn-4").attr('onclick',"faunaSanGuillermo()");
  /* $("#botones").html("");
  htmlHeader = 
  `
  <button id="btn-1" class="btn-activo" onclick="geoSanGuillermo()"><i class="fa fa-globe"></i></button>
  <button id="btn-2" class="btn-desact" onclick="climaSanGuillermo()"><i class="fa fa-cloud"></i></button>
  <button id="btn-3" class="btn-desact" onclick="floraSanGuillermo()"><i class="fa fa-pagelines"></i></button>
  <button id="btn-4" class="btn-desact" onclick="faunaSanGuillermo()"><i class="fa fa-horse"></i></button>
  
  `
  htmlFin = `` 
  $("#botones").html(htmlHeader+htmlFin); */

  $("#descripcion1").html("");
  htmlHeader = 
  `
  <h3 class="titulo"><b>RESERVA DE BIOSFERA - SAN GUILLERMO</b> </h3>
    <div class="descripcion1">
      <p>CATEGORÍA DE PROTECCIÓN: Reserva de La Biósfera</p>
      <p>AUTORIDAD DE APLICACIÓN: La Reserva de Biosfera ‘San Guillermo’ conforma una unidad de conservación
        compuesta
        por dos áreas protegidas de diferente jurisdicción - una provincial y otra nacional -, existiendo a tal fi
        n
        un convenio de manejo integrado de acuerdo a la Ley Nº 25.077.</p>
      <p>OBJETIVO DE CREACIÓN: Conservación de un gran escenario natural escasamente modificado por el hombre
        dónde
        habitan poblaciones importantes de vicuñas y guanacos, especies con diferentes grados de conservación.</p>
      <p>LEY DE CREACIÓN: La Reserva Provincial ‘San Guillermo’ (R.P.S.G.) fue creada por el Decreto Provincial Nº
        2.164 en el año 1972, con una extensión de 981.460 has. En el año 1980, pasa a integrar el Programa
        MAB-UNESCO. En 1997, un sector es cedido al Estado Nacional para la formación del Parque Nacional ‘San
        Guillermo’ (P.N.S.G.), el cuál es creado en 1998 por la Ley Nacional Nº 22.351 para proteger una superfi
        cie
        de 170.000 has.</p>
      <p>SITUACIÓN GEOGRÁFICA EN SAN JUAN: Se encuentra en el extremo Norte de la provincia, sobre el sector
        Noroeste
        del departamento Iglesia, en la región cordillerana. Allí es posible encontrar llanos a 3300 msnm. y picos
        montañosos que superan los 5.000 metros de altura sobre el nivel del mar.</p>
    </div>
  
  `
  htmlFin = `` 
  $("#descripcion1").html(htmlHeader+htmlFin);

  $("#descripcion2").html("");
  htmlHeader = 
  `
  <div class="descripcion2">
      <p>Coordenadas • 29°06′S 69°12′O</p>
      <p>Ecorregiones • Monte de sierras y bolsones, Puna, y Altos Andes</p> 
      <p>La Reserva de Biósfera se encuentra en el extremo Noroeste de la provincia de San Juan, abarcando
        aproximadamente al 45% del departamento de Iglesia. Limita al Oeste con la Cordillera de Los Andes (límite
        con
        Chile); al Norte con la provincia de La Rioja; al Este con el Río Blanco hasta la confl uencia con el Río de
        La
        Palca; y al Sur con la línea imaginaria que resulta de unir el Paso de las Tórtolas (en el límite
        internacional
        argentino - chileno) con el punto de confl uencia entre el Río de La Palca y el Río Blanco. Su centro
        geográfi
        co aproximado está a 29° 10’ de latitud Sur y 69° 20’ de longitud Oeste.</p>
       </div>
  
  `
  htmlFin = `` 
  $("#descripcion2").html(htmlHeader+htmlFin);
  activarGeo();

  /* $('#img-principal').attr("src","src/img/algarrobo.png");
  $('#img1').attr("src","src/img/Imagen1.png"); */
  
  
});

//////////////////////////////////////////////////////////  ISCHIGUALASTO   //////////////////////////////////////////////////////

document.getElementById("Parque_Provincia_Ischigualasto").addEventListener("click",function( event ) {
  window.location.href = '#prueba';
  activarGeo();
  $("#prueba").html("");
  let htmlHeader = 
  `
  <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d871783.9010242174!2d-68.1441978635602!3d-30.15499699233426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9682ec3d05e629c3%3A0xcce54efd17759dd0!2sParque%20Provincial%20Ischigualasto!5e0!3m2!1ses!2sar!4v1642172348496!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

  `
  let htmlFin = `` 
  $("#prueba").html(htmlHeader+htmlFin);
  $("#btn-1").attr('onclick',"geoIschigualasto()");
  $("#btn-2").attr('onclick',"climaIschigualasto()");
  $("#btn-3").attr('onclick',"floraIschigualasto()");
  $("#btn-4").attr('onclick',"faunaIschigualasto()");
  /* $("#botones").html("");
  htmlHeader = 
  `
  <button id="btn-1" class="btn-activo" onclick="geoIschigualasto()"><i class="fa fa-globe"></i></button>
  <button id="btn-2" class="btn-desact" onclick="climaIschigualasto()"><i class="fa fa-cloud"></i></button>
  <button id="btn-3" class="btn-desact" onclick="floraIschigualasto()"><i class="fa fa-pagelines"></i></button>
  <button id="btn-4" class="btn-desact" onclick="faunaIschigualasto()"><i class="fa fa-horse"></i></button>
  
  `
  htmlFin = `` 
  $("#botones").html(htmlHeader+htmlFin); */

  $("#descripcion1").html("");
  htmlHeader = 
  `
  <h3 class="titulo"><b>PARQUE PROVINCIAL - ISCHIGUALASTO</b> </h3>
    <div class="descripcion1">
      <p>CATEGORÍA DE PROTECCIÓN: Parque Natural y Patrimonio Natural de la Humanidad.</p>
      <p>AUTORIDAD DE APLICACIÓN: Ente Autárquico Ischigualasto.</p>
      <p>OBJETIVO DE CREACIÓN: Proteger un importante yacimiento paleontológico que se caracteriza por la abundancia de restos fósiles de la fauna del Triásico. 
      Preservar una muestra de su naturaleza actual, característica de la provincia biogeográfi ca del Monte. Proteger un paisaje de gran belleza escénica. 
      Brindar un espacio silvestre con yacimientos paleontológicos.</p>
      <p>LEY Y AÑO DE CREACIÓN: Ley Provincial Nº 3.666; Año 1971 y Declaración de Patrimonio Natural de la Humanidad por la UNESCO; Año 2000.</p>
      <p>UBICACIÓN GEOGRÁFICA EN SAN JUAN: El Parque se encuentra localizado en el Nordeste de la provincia de San Juan, en los departamentos de Jáchal y Valle Fértil, limitando con el Parque Nacional Talampaya, La Rioja. 
      Su centro geográfi co aproximado es Latitud Sur 29º 55´ y Longitud Oeste 68º 05´.</p>
      <p>SUPERFICIE: 62.369 has.</p>

      
    </div>
  
  `
  htmlFin = `` 
  $("#descripcion1").html(htmlHeader+htmlFin);

  $("#descripcion2").html("");
  htmlHeader = 
  `
  <div class="descripcion2">
      <p>Coordenadas	•	29° 55 LS y 68° 05 LO</p>
      <p>Ecorregiones	•	Monte de llanuras y mesetas.</p> 
      <p>Ischigualasto se encuentra localizado a 330 Km. de la Ciudad de San Juan, en el sector Nordeste de la provincia, abarcando parte de los departamentos Jáchal y Valle Fértil. 
      El Parque comprende 60.369 hectáreas, en su gran mayoría dispuestas en Valle Fértil y otras 2000 has. en el departamento Jáchal. 
      Se encuentra ubicado con un centro geográfico aproximado de su territorio se dispone entre un rango altitudinal de 1200 msnm en las partes más bajas de la formación ‘Ischigualasto’ y de 1900 msnm en las cumbres de las Sierras de Valle Fértil.</p>
       </div>
  
  `
  htmlFin = `` 
  $("#descripcion2").html(htmlHeader+htmlFin);

  /* $('#img-principal').attr("src","src/img/algarrobo.png");
  $('#img1').attr("src","src/img/Imagen1.png"); */

  
});

//////////////////////////////////////////////////  PARQUE NACIONAL VALLE FERTIL   //////////////////////////////////////////////////////

document.getElementById("PNVF").addEventListener("click",function( event ) {
  window.location.href = '#prueba';
  activarGeo();
  $("#prueba").html("");
  let htmlHeader = 
  `
  <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d777585.8986804273!2d-68.12081232839145!3d-31.292226356999794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9681f433bfc8e411%3A0xbf088efded1b4c34!2sReserva%20de%20uso%20m%C3%BAltiple%20Valle%20F%C3%A9rtil!5e0!3m2!1ses!2sar!4v1642420280140!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

  `
  let htmlFin = `` 
  $("#prueba").html(htmlHeader+htmlFin);
  $("#btn-1").attr('onclick',"geoValleFertil()");
  $("#btn-2").attr('onclick',"climaValleFertil()");
  $("#btn-3").attr('onclick',"floraValleFertil()");
  $("#btn-4").attr('onclick',"faunaValleFertil()");
  /* $("#botones").html("");
  htmlHeader = 
  `
  <button id="btn-1" class="btn-activo" onclick="geoValleFertil()"><i class="fa fa-globe"></i></button>
  <button id="btn-2" class="btn-desact" onclick="climaValleFertil()"><i class="fa fa-cloud"></i></button>
  <button id="btn-3" class="btn-desact" onclick="floraValleFertil()"><i class="fa fa-pagelines"></i></button>
  <button id="btn-4" class="btn-desact" onclick="faunaValleFertil()"><i class="fa fa-horse"></i></button>
  
  `
  htmlFin = `` 
  $("#botones").html(htmlHeader+htmlFin);
*/
  $("#descripcion1").html("");
  htmlHeader = 
  `
  <h3 class="titulo"><b>RÉGIMEN DE PROTECCIÓN: Reserva De Usos Múltiples</h3>
    <div class="descripcion1">
      <p>AUTORIDAD DE APLICACIÓN: Subsecretaría de Conservación y Áreas Protegidas; Secretaría de Ambiente y Desarrollo Sustentable.</p>
      <p>LEY Y AÑO DE CREACIÓN: Ley Provincial Nº 3. 666; Año 1971.</p>
      <p>SITUACIÓN GEOGRÁFICA EN SAN JUAN: Centro - Este de la provincia, abarcando partes del valle de Ampacama, de las sierras de Pie de Palo y de las sierras de Valle Fértil - de la Huerta, incluyendo al pie de monte oriental de esta última (Valle Fértil, Caucete, Angaco y Jáchal). 
      </p>
      <p>SUPERFICIE: 8.000 km. cuadrados. 
      </p>
      <p>CARACTERÍSTICAS PRINCIPALES:</p>
      <p>• Se trata de un área de gran interés biogeográfi co por su carácter 
      ecotonal, mostrando la transición del Monte con el Chaco (Serrano y Árido).</p>
      <p>• Es uno de los lugares con mayor riqueza natural de la provincia por albergar a muchas especies que no se encuentran en el resto de territorio, por ejemplo el Rey del Bosque.</p>
      <p>• En las serranías se conservan bosquecillos de Chica (Ramorinoa girolae), endemismo argentino.</p>

    </div>
  
  `
  htmlFin = `` 
  $("#descripcion1").html(htmlHeader+htmlFin);

  $("#descripcion2").html("");
  htmlHeader = 
  `
  <div class="descripcion2">
      <p>Coordenadas	•	30° 30' S y 67° 26 O</p>
      <p>Ecorregiones	•	Monte de llanuras y mesetas.</p> 
      <p>El Parque Natural Valle Fértil se encuentra ubicado en el Centro - Este de la provincia, abarcando parte de los departamentos Caucete, Angaco, Jáchal y Valle Fértil. 
      Este espacio protegido incluye partes del valle de Ampacama, de las sierras de Pie de Palo, de las sierras de Valle Fértil - de la Huerta y del pie de monte oriental de esta última. </p>
      <p>La superficie del Parque está determinada por los siguientes límites: Al Sur, con Ruta Nacional Nº 20, partiendo desde el meridiano 68 (altura Vallecito) hasta Marayes. 
      Al Este, desde la confluencia de la Ruta Nacional Nº 20 con la Ruta Provincial Nº 293, la que pasa por San Agustín (desarrollo 80 Km. aproximadamente) hasta Usno. 
      Al Norte, desde la Ruta Provincial Nº 293, altura de Usno, y la intersección del paralelo 30º 30’ con el meridiano 68º, siguiendo por este último hasta el encuentro con la Ruta Nacional Nª 20 (altura Vallecito). 
      Superficie aproximada del polígono: 800.000 hectáreas.</p> 
      <p>ACCESOS VIALES: </p> 
      <p>Desde la Ciudad de San Juan por Ruta Nacional Nº 141 hasta la localidad de Marayes, y desde esa localidad por Ruta Prov. Nº 293 hasta la villa cabecera de San Agustín de Valle Fértil.</p> 
      <p>Desde la provincia de La Rioja: Por Ruta Nac. Nº 38 hasta Patquía. 
      Desde Patquía al Valle de la Luna, por Ruta Prov. Nº 150; y desde el Valle de la Luna hasta la Villa San Agustín, por Ruta Prov. Nº 293. </p>
      </div>
  
  `
  htmlFin = `` 
  $("#descripcion2").html(htmlHeader+htmlFin);

  /* $('#img-principal').attr("src","src/img/algarrobo.png");
  $('#img1').attr("src","src/img/Imagen1.png"); */

  
});

///////////////////////////////////////////////////////////  LA CIENAGA   //////////////////////////////////////////////////////

document.getElementById("La_Cienaga").addEventListener("click",function( event ) {
  window.location.href = '#prueba';
  activarGeo();
  $("#prueba").html("");
  let htmlHeader = 
  `
  <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27600.724033408438!2d-68.59577297186266!3d-30.148828377880168!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9683404c0105b065%3A0xaee8e235e041b3ba!2sLa%20Ci%C3%A9naga%2C%20San%20Juan!5e0!3m2!1ses!2sar!4v1642424563932!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

  `
  let htmlFin = `` 
  $("#prueba").html(htmlHeader+htmlFin);
  $("#btn-1").attr('onclick',"geoLaCienaga()");
  $("#btn-2").attr('onclick',"climaLaCienaga()");
  $("#btn-3").attr('onclick',"floraLaCienaga()");
  $("#btn-4").attr('onclick',"faunaLaCienaga()");
  /* $("#botones").html("");
  htmlHeader = 
  `
  <button id="btn-1" class="btn-activo" onclick="geoLaCienaga()"><i class="fa fa-globe"></i></button>
  <button id="btn-2" class="btn-desact" onclick="climaLaCienaga()"><i class="fa fa-cloud"></i></button>
  <button id="btn-3" class="btn-desact" onclick="floraLaCienaga()"><i class="fa fa-pagelines"></i></button>
  <button id="btn-4" class="btn-desact" onclick="faunaLaCienaga()"><i class="fa fa-horse"></i></button>
  
  `
  htmlFin = `` 
  $("#botones").html(htmlHeader+htmlFin); */

  $("#descripcion1").html("");
  htmlHeader = 
  `
  <h3 class="titulo"><b>CATEGORÍA DE PROTECCIÓN: Área Natural Protegida </h3>
    <div class="descripcion1">
      <p>AUTORIDAD DE APLICACIÓN: Secretaría de Ambiente y Desarrollo Sustentable; Subsecretaría de Conservación y Áreas Protegidas.</p>
      <p>LEY Y AÑO DE CREACIÓN: Ley Nº 7640 - 2005</p>
      <p>SITUACIÓN GEOGRÁFICA EN SAN JUAN: Localidad de La Ciénaga, departamento Jáchal; a 25 km. de su Villa Cabecera y a 10 km. de la localidad de Huaco.  
      </p>
      <p>SUPERFICIE: 9.600 has. 
      </p>
      
    </div>
  
  `
  htmlFin = `` 
  $("#descripcion1").html(htmlHeader+htmlFin);

  $("#descripcion2").html("");
  htmlHeader = 
  `
  <div class="descripcion2">
      <p>Coordenadas	•	30° 08' S y 68° 34' O</p>
      <p>Ecorregiones	•	Monte</p> 
      <p>Perteneciente al departamento Jáchal, a 25 km de su villa cabecera, La Ciénaga está ubicada en la Quebrada del Río Huaco, cuyas aguas atraviesan la reserva de Oeste a Este. La zona se encuentra inserta en un marco de sierras de mediana altura que corresponden - geológicamente - a la Precordillera Central. 
      Al Oeste, limita con el Dique Los Cauquenes y Cerro El Perico; al Este, está al límite de la Cuesta Colorada - Represa de Huaco; al Norte, el Alto Las Azucenas; y al Sur, limita con las Torres del Portezuelo o los Llanos Amarillos.</p>
      <p>ACCESOS </p> 
      <p>Desde la ciudad de San Juan se accede por la Ruta Nacional Nº 491. Saliendo de la villa cabecera (San José de Jáchal), al Noreste, y pasando la localidad de Pampa Vieja, el viajero se encuentra con un gran espejo de agua: el Dique Los Cauquenes. 
      Luego, se accede por un camino de cornisa zigzagueante y angosto (La Cuesta) que alcanza los mil metros de altura sobre el nivel del mar. Al pasar el túnel, se observan las primeras casas de los pobladores de La Ciénaga. La localidad conecta a San José de Jáchal con Huaco.</p> 
      
      </div>
  
  `
  htmlFin = `` 
  $("#descripcion2").html(htmlHeader+htmlFin);

  /* $('#img-principal').attr("src","src/img/algarrobo.png");
  $('#img1').attr("src","src/img/Imagen1.png"); */

  
});
/////////////////////////////////////////////////////////   DON CARMELO   //////////////////////////////////////////////////////
      
document.getElementById("Don_Carmelo").addEventListener("click",function( event ) {
  window.location.href = '#prueba';      
  activarGeo();
        $("#prueba").html("");
        let htmlHeader = 
        `
        <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d491387.8164584714!2d-69.34148949623143!3d-30.994723859470124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x968472bd380b504b%3A0xb7efc26c3f451770!2sReserva%20Don%20Carmelo!5e0!3m2!1ses!2sar!4v1642430207624!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        `
        let htmlFin = `` 
        $("#prueba").html(htmlHeader+htmlFin);
        $("#btn-1").attr('onclick',"geoDonCarmelo()");
        $("#btn-2").attr('onclick',"climaDonCarmelo()");
        $("#btn-3").attr('onclick',"floraDonCarmelo()");
        $("#btn-4").attr('onclick',"faunaDonCarmelo()");
        /* $("#botones").html("");
        htmlHeader = 
        `
        <button id="btn-1" class="btn-activo" onclick="geoDonCarmelo()"><i class="fa fa-globe"></i></button>
        <button id="btn-2" class="btn-desact" onclick="climaDonCarmelo()"><i class="fa fa-cloud"></i></button>
        <button id="btn-3" class="btn-desact" onclick="floraDonCarmelo()"><i class="fa fa-pagelines"></i></button>
        <button id="btn-4" class="btn-desact" onclick="faunaDonCarmelo()"><i class="fa fa-horse"></i></button>
        
        `
        htmlFin = `` 
        $("#botones").html(htmlHeader+htmlFin); */

        $("#descripcion1").html("");
        htmlHeader = 
        `
        <h3 class="titulo"><b>CATEGORÍA: Reserva De Uso Múltiple</h3>
          <div class="descripcion1">
            <p>RÉGIMEN DE PROTECCIÓN: Reserva Natural Privada.</p>
            <p>OBJETIVO DE CREACIÓN: Conservar muestras de las comunidades del Distrito Cuyano de la provincia Altoandina y de la provincia Puneña.
             Además, privilegiar la convivencia armónica entre las actividades productivas antrópicas y el mantenimiento de los ambientes naturales con sus recursos silvestres. </p>
            <p>LEY Y AÑO DE CREACIÓN: Por convenio entre el Gobierno provincial y sus propietarios. Decreto Provincial 1220; Año 1993.
            </p>
            <p>SITUACIÓN GEOGRÁFICA EN SAN JUAN: Centro - Oeste del departamento Ullum, en el centro de la provincia, y a 130 km. de la ciudad de San Juan, sobre el límite con el departamento Calingasta. 
            </p>
            <p> SUPERFICIE: 40.000 has.</p>
            <p>CARACTERÍSTICAS PRINCIPALES:</p>
            <p>• En la reserva se encuentran elementos de las ecorregiones del Monte y la Puna, apreciándose también algunas especies altoandinas. La fi sonomía dominante es la de un matorral abierto que decrece en altitud hasta aproximadamente 3.000 m.s.n.m., por encima del cuál se establece el pastizal de coirones.</p>
            <p>CARACTERÍSTICAS PRINCIPALES:</p>
            <p>• En el área habitan varias especies en distinto grado de amenaza, como el suri cordillerano, el cóndor andino, el águila mora, el halcón peregrino, el puma, el chinchillón y el guanaco.</p>
            <p>• Se implementa en el área un uso turístico de corto tiempo, el que aprovecha las imponentes vistas panorámicas de las cumbres que delimitan la reserva.</p>
            
            </div>
        
        `
        htmlFin = `` 
        $("#descripcion1").html(htmlHeader+htmlFin);

        $("#descripcion2").html("");
        htmlHeader = 
        `
        <div class="descripcion2">
            <p>Coordenadas	•	31° 18' S y 68° 30 O</p>
            <p>Ecorregiones	•	Puna</p> 
            <p>La Reserva Privada de Uso Múltiple Don Carmelo está ubicada en el Valle de la Invernada, en el Centro - Oeste del departamento Ullum; a los 31°, 18’, 40’’ latitud Sur; y a 68°, 30’, 40’’ longitud Oeste; a una altura aproximada de 3.000 m.s.n.m. Se encuentra enclavada en la precordillera de la Provincia de San Juan, a 130 Km. aproximadamente de la Capital de San Juan.</p> 
            <p>Los límites de la Reserva son: al Sur, el Río San Juan; al Oeste, las Sierras del Tigre; al Este, las Sierras de la Invernada (que le dan nombre al valle). El Norte está constituido por una línea imaginaria que corre de Este a Oeste a la altura del Cerro Morterito.</p> 
            <p>ACCESOS:</p>
            <p>A la Estancia Don Carmelo se accede por Ruta Nacional Nº 40 hasta Talacasto. Desde allí se toma la Ruta Provincial Nº 436, hacia Iglesia y hasta la altura del puesto de Vialidad llamado “La Ciénaga” ( aprox. a 107 km de la ciudad de San Juan). Luego, se transita por una huella de unos 34 Km. que lleva a la Reserva (sólo para vehículos 4 x 4). Un poco más adelante, pasando la mina de Gualilán, se encuentra otra huella, de unos 36 Km., para todo tipo de vehículos.</p>
            
            </div>
        
        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);


        
      });

//////////////////////////////////////////////////   Paisaje Protegido Pedernal   ///////////////////////////////////////////////
      
document.getElementById("Pedernal").addEventListener("click",function( event ) {
  window.location.href = '#prueba';    
  activarGeo();
              $("#prueba").html("");
              let htmlHeader = 
              `
              <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4023.925376254152!2d-68.76631270528621!3d-31.99602718077817!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9680d79135259d5d%3A0xb7d5ba70f8d2e3ec!2sPedernal%2C%20San%20Juan!5e0!3m2!1ses!2sar!4v1642771492444!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              `
              let htmlFin = `` 
              $("#prueba").html(htmlHeader+htmlFin);

              $("#prueba").html(htmlHeader+htmlFin);
              $("#btn-1").attr('onclick',"geoPedernal()");
              $("#btn-2").attr('onclick',"climaPedernal()");
              $("#btn-3").attr('onclick',"floraPedernal()");
              $("#btn-4").attr('onclick',"faunaPedernal()");


              /* $("#botones").html("");

              htmlHeader = 
              `
              <button id="btn-1"  class="btn-activo" onclick="geoPedernal()"><i class="fa fa-globe"></i></button>
              <button id="btn-2" class="btn-desact" onclick="climaPedernal()"><i class="fa fa-cloud"></i></button>
              <button id="btn-3" class="btn-desact" onclick="floraPedernal()"><i class="fa fa-pagelines"></i></button>
              <button id="btn-4" class="btn-desact" onclick="faunaPedernal()"><i class="fa fa-horse"></i></button>
              
              `
              htmlFin = `` 
              $("#botones").html(htmlHeader+htmlFin); */
      
              $("#descripcion1").html("");
              htmlHeader = 
              `
              <h3 class="titulo"><b>NOMBRE DEL ANP: Localidad de Pedernal</h3>
                <div class="descripcion1">
                  <p>CATEGORÍA DE PROTECCIÓN: Paisaje Protegido</p>
                  <p>AUTORIDAD DE APLICACIÓN: Secretaría de Ambiente y Desarrollo Sustentable; Subsecretaría de Conservación y Áreas Protegidas.</p>
                  <p>OBJETIVO DE CREACIÓN: El objetivo de creación de este área fue conservar las características naturales del paisaje y proteger el hábitat de especies silvestres.</p>
                  <p>LEY Y AÑO DE CREACIÓN: Ley Provincial Nº 7028; Año 2000 y Ley Provincial Nº 7766; Año 2006.</p>
                  <p>UBICACIÓN GEOGRÁFICA EN SAN JUAN: Pedernal está ubicada al Sur - Oeste de la Provincia de San Juan, en el departamento Sarmiento.</p>
                  <p>SUPERFICIE: 17.700 has.</p>
                  <p>CARACTERÍSTICAS PRINCIPALES</p>
                  <p>• La vegetación de Pedernal corresponde a las provincias fitogeográficas del Monte y de la Prepuna, cubriendo la primera más del 80% del área. Se destacan grandes jarillales en los pedemontes y los chaguarales en los afloramientos rocosos.</p>
                  <p>• El área de Pedernal posee un antiguo uso ganadero de finales del Siglo XVI, situación que infl uyó en la introducción de especies exóticas y la modificación de la vegetación natural por efecto del fuego.</p>
                  </div>
              
              `
              htmlFin = `` 
              $("#descripcion1").html(htmlHeader+htmlFin);
      
              $("#descripcion2").html("");
              htmlHeader = 
              `
              <div class="descripcion2">
                  <p>Coordenadas	•	31° 59' S y 68° 45' O</p>
                  <p>Ecorregiones	•	Altos Andes - Monte de Sierras y Bolsones</p> 
                  <p>El Valle del Pedernal se ubica al Sur de la Provincia de San Juan, en el departamento Sarmiento, cerca del límite con la Provincia de Mendoza. Yace recostado en el pedemonte de la Precordillera, a una altura aproximada de 1.240 msnm. Se encuentra en el Km. 38 de la Ruta Nacional 153 y a 107 Km. de la ciudad de San Juan. El área está inmersa en la Precordillera , delimitada al Oeste por las sierras del Tontal y al Sureste por el cordón de Pedernal.</p> 
                  <p>ACCESOS:</p> 
                  <p>Se puede acceder desde la Ciudad de San Juan por la Ruta Nacional Nº 40 hasta la localidad de Media Agua. Continuando por la Ruta Provincial Nº 153, se accede a las localidades de Cañada Honda y Los Berros hasta llegar al Dique Las Crucecitas. </p>
                  <p>Otra forma de acceso es por el departamento Calingasta. Recorriendo la Ruta Provincial Nº 412, se transpone la localidad de Barreal y el Parque Nacional El Leoncito hasta acceder a la Ruta Provincial Nº 153, que ingresa a Pedernal por el Oeste.</p>
                  <p>Desde Mendoza también se puede llegar por Ruta Nacional Nº 40 y por la Ruta Provincial Nº 39, que une Uspallata con Barreal</p>
                  
                  </div>
              
              `
              htmlFin = `` 
              $("#descripcion2").html(htmlHeader+htmlFin);
      
      
              
});

/////////////////////////////////////////////////////   FUNCIONES BOTONES   /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Parque_Natural_Manantiales
////////////////////////////////////////////////////////   MANANTIALES   ///////////////////////////////////////////////////////////////
function climaManantiales(){
  activarClima();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="..." class="imagenes d-block w-100" alt="...">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">

       
       


          <p>El clima y el relieve son los factores ambientales principales que condicionan a la distribución de las unidades ecológicas y de la biodiversidad.</p>
          <p>El clima local es el resultado de numerosos y complejos factores. De ellos, cabe citar a la distancia a las fuentes oceánicas, la orografía, la ubicación y el desplazamiento de centros de alta y baja presión, la altura con respecto al nivel del mar, etc. Se caracteriza principalmente por ser un ámbito áridodesértico, con grandes amplitudes térmicas diurnas-nocturnas y anuales (verano-invierno). Se registra una elevada heliofanía e insolación con una importante transparencia atmosférica y escasa humedad. Se presentan lluvias - exclusivamente estivales - en el sector deprimido entre Precordillera y Cordillera Frontal. Hay precipitaciones sólidas invernales (principalmente en forma de nieve) en la zona cordillerana, con una muy baja frecuencia 
          media de días con precipitaciones (sean éstas líquidas o sólidas). Las temperaturas son bajas la mayor parte del año, en particular para los relieves elevados.</p> 
          
           </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);


      
}

function faunaManantiales(){
  activarFauna();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="..." class="imagenes d-block w-100" alt="...">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      






      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">
          <p>El sistema protegido de San Guillermo contiene especies con un variado interés de conservación por albergar la fauna de tres ecorregiones y ser el único sitio en la Sudamérica árida, que posee un ensamble completo de la fauna nativa. El hecho más destacado en esta Reserva de Biósfera es la coexistencia de importantes núcleos poblacionales de dos grandes camélidos: Vicuñas (Vicugna vicugna), constituyendo la población más austral del mundo de esta especie y la de mayor concentración del país; y Guanacos (Lama guanicoe). Ambos utilizan distintos hábitats para así disminuir la competencia: los llanos para la vicuña, y los faldeos rocosos para el guanaco. Al igual que la fl ora, la fauna del área presenta adaptaciones al medio. </p>
          <p>Así, estos herbívoros se adaptaron a la altura y a la aridez del ambiente, desarrollando unas almohadillas en sus patas que minimizan el efecto erosivo del pisoteo. Además, poseen una dentadura que corta los pastos sin arrancarlos, lo que también facilita su rebrote. </p> 
          <p>La Reserva de Biósfera se encuentra en el extremo Noroeste de la provincia de San Juan, abarcando
            aproximadamente al 45% del departamento de Iglesia. Limita al Oeste con la Cordillera de Los Andes (límite
            con
            Chile); al Norte con la provincia de La Rioja; al Este con el Río Blanco hasta la confl uencia con el Río de
            La
            Palca; y al Sur con la línea imaginaria que resulta de unir el Paso de las Tórtolas (en el límite
            internacional
            argentino - chileno) con el punto de confl uencia entre el Río de La Palca y el Río Blanco. Su centro
            geográfi
            co aproximado está a 29° 10’ de latitud Sur y 69° 20’ de longitud Oeste.</p>
            <p>San Guillermo posee un alto número de endemismos; es decir, especies con una distribución restringida a un área determinada. Las especies endémicas documentadas hasta el momento incluyen dos reptiles: el Chelco de San Guillermo (Liolaemus eleodori) y el Cola de Piche de San Guillermo (Phymaturus punae). Ello también se da con los mamíferos: el Ratón de la Vega (Neotomys ebriosus) y la Rata Chinchilla (Abrocoma cinerea).También poseería una de las poblaciones más australes del amenazado Gato Andino (Oreailurus jacobita), usualmente llamado Gato Onza. La presencia de este felino fue documentada por WCS (Wildlife Conservation Society) en 2004. Los esfuerzos de investigación de WCS también proporcionaron los 
            primeros registros para el área de otras especies de carnívoros: el Gato de Pajonal (Oncifelis colocolo), el Zorro Colorado (Lycalopex culpeus) y el Zorro Gris Chico (Lycalopex gimnocercus). Incluso podemos encontrar al Puma (Puma concolor), el carnívoro de mayor tamaño presente en el área y visible a pleno día. Hay otras especies de mamíferos con un registro incompleto, incluyendo a la altamente amenazada Chinchilla de Cola Corta (Chinchilla brevicaudata). </p>
            <p>Se registra una gran variedad de roedores, como la Vizcacha de la Sierra o Chinchillón (Lagidium viscacia) - presentes en quebradas y lugares abiertos - y el oculto Tuco-Tuco (Ctenomys sp.), el que suele refugiarse en cuevas o galenas de piedras para protegerse de las inclemencias del clima.</p>
            <p>San Guillermo registra una gran diversidad de aves: El Suri Cordillerano (Pterocnemia tarapacensis), especie amenazada a nivel nacional, que en el área recorre en grupos las pampas de altura; la Quiula Puneña o Keu Andino (Tinamotis penlandii), similar a la Martineta Común, pero sin copete y también amenzada; el Flamenco Austral (Phoenicopterus chilensis); el Matamico Andino (Phalcoboenus megalopterus); la Guayata o Piuquén (Chloephaga melanoptera); el Cóndor Andino (Vultur gryphus); el Carancho (Caracara plancus); la Gallareta Cornuda (Fulica cornuta); el Batitú (Bartramia longicauda), de la familia de los playeritos; la Agachona de Collar o Puco-Puco de Altura (Thinocorus orbignyianus); la Agachona Grande (Attagis gayi); la Palomita Dorada o de la Sierra (Metriopelia aymara); el Piojito Gris (Serpophaga nigricans) y el Comesebo Andino o Boquense (Prygilus gayi). Los peces nativos están representados por una única especie, el Pique (Hatchería macraeí), una especie de pez gato que alcanza 21 cm y es endémica de los ríos andinos de Argentina.</p>
          
          
            </div>

      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);

}

function floraManantiales(){
  activarFlora();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="..." class="imagenes d-block w-100" alt="...">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 


      $("#descripcion2").html("");


      htmlHeader = 
      `
      <div class="descripcion2">
          <p>UNIDADES FITOGEOGRÁFICAS</p>
          <p>Hay tres provincias fitogeográficas presentes en San Guillermo: Monte, Puna y Altoandina, con transiciones entre ellas que varían según la exposición y la pendiente (de Este a Oeste, se asciende de los 1.700 m a más de 5.000 m de altura). El Cardonal, si bien no se encuentra incluido en la misma área protegida, aparece en las cercanías en laderas de exposición Norte ni bien se comienza el ascenso por la vertiente oriental de los Andes.            </p>
          <p>La ecorregión ALTOANDINA se presenta por sobre los 4000 msnm. y hasta los 4400 metros (límite de vegetación en el área). Se caracteriza por presentar vegetación en cojines. Allí domina el género Adesmias (Adesmia sp.) y elementos acompañantes como el Coirón (Stipa frígida), entre otros. </p> 
          <p>La ecorregión PUNEÑA se presenta en la RBSG con variadas fi sonomías y es el piso más importante en cuanto a extensión, ubicándose entre los 3.000 y 3.800 msnm. También hay un ecotono de piso superior al Altoandino, variable en extensión según topografía, entre 3.800 y 4.200 msnm. </p>
          <p>Entre las comunidades vegetales puneñas, se han identifi cado las siguientes:</p>
          <p>a) Matorrales de Ajenjo (Artemisia sp), Lycium (Lycium chañar), Tola (Fabiana punensis), Jarilla (Larrea divaricata), Pinchagua (Lycium fuscum) y Adesmia pinifolia</p>
          <p>b) Monte Negro (Trycicla spinosa) </p>
          <p>c) Pastizales de Coirón (Stipa chrysophylla var. chrysophylla, de Stipa speciosa var. Abscondita) </p>
          <p>La ecorregión del MONTE se encuentra representada en la zona Sur de la RBSG hasta los 2800 msnm. Se caracteriza por presentar matorrales abiertos, particularmente en fondos de valles. Las especies propias que identifi can este ambiente en el área son varias: Algarrobo Dulce (Prosopis 
            fl exuosa var. Depressa), Jarilla (Larrea divaricata), Retamo 
            (Bulnesia retama), etc.</p>
          <p>VEGETACIÓN DE VEGAS</p>
          <p>Las vegas son los ambientes de mayor importancia para la biodiversidad. El porcentaje de cobertura vegetal y la biomasa aérea es signifi cativamente mayor que en otros ambientes de altura. Así también, las vegas presentan el mayor número de especies vegetales, considerando que se encuentran tanto en el piso puneño como en el altoandino. Así, posee diferentes cinturones de vegetación de acuerdo al grado de saturación de los suelos. </p>
          <p>Se identifican cinco comunidades de vegetación:</p>
          <p>1- Juncus arcticus</p>
          <p>2- Carex</p>
          <p>3- Potasia clandestina</p>
          <p>4- Juncus balticus</p>
          <p>5- Potamogeton pectinatus</p>
          <p>6- Aquellas de base granítica, con presencia de Urtica buchtienii</p>
          <p>Las vegas tienen una importancia particular para la avifauna. También tiene gran relevancia como hábitat para el único representante de los anfibios conocido en el área: el Sapo Andino (Bufo spinolosus), razón por la cual esta especie podría ser un buen indicador de contaminación de las aguas. Por otro lado, numerosos estudios han confi rmado la dependencia de los camélidos respecto de estos ambientes. La presencia de la juncácea Oxychloe sp. como segundo componente relevante en la dieta de la vicuña y presente únicamente en las vegas de altura, podría indicar que esta planta tiene un papel destacado en la distribución de la especie. </p>
          <p>Para los predadores, las vegas también constituyen un ambiente de preferencia, ya que son sitios ideales para la caza - respectivamente - de camélidos. </p>
          <p>Las juncáceas son escondites efi caces para el Puma (Puma concolor) al momento de acechar sus presas. Ya que los camélidos representan el principal ítem en la dieta del puma en San Guillermo, puede inferirse que los ambientes de vega resultan vitales en la dinámica predatoria de estos carnívoros.</p>
          <p>Hay que destacar también que el área posee un antiguo uso ganadero que se remonta a los períodos prehispánico y fi nales del Siglo XVI, el cual ha tenido una infl uencia directa en la introducción de especies exóticas como el Burro (Equus africanus asinus), el Caballo (Equus ferus caballus) y la Oveja (Ovis orientalis aries).</p>

          </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);


}
function geoManantiales(){
  activarGeo();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d890098.440705852!2d-69.3430181219542!3d-29.371840026116722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x969b420fe75eac21%3A0x18f6eae9c8b423f3!2sParque%20Nacional%20San%20Guillermo!5e0!3m2!1ses!2sar!4v1640614166409!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">
          <p>Coordenadas • 29°06′S 69°12′O</p>
          <p>Ecorregiones • Monte de sierras y bolsones, Puna, y Altos Andes</p> 
          <p>La Reserva de Biósfera se encuentra en el extremo Noroeste de la provincia de San Juan, abarcando
            aproximadamente al 45% del departamento de Iglesia. Limita al Oeste con la Cordillera de Los Andes (límite
            con
            Chile); al Norte con la provincia de La Rioja; al Este con el Río Blanco hasta la confl uencia con el Río de
            La
            Palca; y al Sur con la línea imaginaria que resulta de unir el Paso de las Tórtolas (en el límite
            internacional
            argentino - chileno) con el punto de confl uencia entre el Río de La Palca y el Río Blanco. Su centro
            geográfi
            co aproximado está a 29° 10’ de latitud Sur y 69° 20’ de longitud Oeste.</p>
           </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);
}
////////////////////////////////////////////////////////   SAN GUILLERMO   ///////////////////////////////////////////////////////////////
function climaSanGuillermo(){
  activarClima();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="./src/img/San-Guillermo/Clima-1.jpg" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Clima-2.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Clima-3.png" class="imagenes d-block w-100" alt="...">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">

       
       


          <p>El clima y el relieve son los factores ambientales principales que condicionan a la distribución de las unidades ecológicas y de la biodiversidad.</p>
          <p>El clima local es el resultado de numerosos y complejos factores. De ellos, cabe citar a la distancia a las fuentes oceánicas, la orografía, la ubicación y el desplazamiento de centros de alta y baja presión, la altura con respecto al nivel del mar, etc. Se caracteriza principalmente por ser un ámbito áridodesértico, con grandes amplitudes térmicas diurnas-nocturnas y anuales (verano-invierno). Se registra una elevada heliofanía e insolación con una importante transparencia atmosférica y escasa humedad. Se presentan lluvias - exclusivamente estivales - en el sector deprimido entre Precordillera y Cordillera Frontal. Hay precipitaciones sólidas invernales (principalmente en forma de nieve) en la zona cordillerana, con una muy baja frecuencia 
          media de días con precipitaciones (sean éstas líquidas o sólidas). Las temperaturas son bajas la mayor parte del año, en particular para los relieves elevados.</p> 
          
           </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);


      
}

function faunaSanGuillermo(){
  activarFauna();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="./src/img/San-Guillermo/Fauna-1.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Fauna-2.png" class="imagenes d-block w-100" alt="hola">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Fauna-3.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Fauna-4.png" class="imagenes d-block w-100" alt="hola">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Fauna-5.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Fauna-6.png" class="imagenes d-block w-100" alt="hola">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Fauna-7.png" class="imagenes d-block w-100" alt="hola">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      






      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">
          <p>El sistema protegido de San Guillermo contiene especies con un variado interés de conservación por albergar la fauna de tres ecorregiones y ser el único sitio en la Sudamérica árida, que posee un ensamble completo de la fauna nativa. El hecho más destacado en esta Reserva de Biósfera es la coexistencia de importantes núcleos poblacionales de dos grandes camélidos: Vicuñas (Vicugna vicugna), constituyendo la población más austral del mundo de esta especie y la de mayor concentración del país; y Guanacos (Lama guanicoe). Ambos utilizan distintos hábitats para así disminuir la competencia: los llanos para la vicuña, y los faldeos rocosos para el guanaco. Al igual que la fl ora, la fauna del área presenta adaptaciones al medio. </p>
          <p>Así, estos herbívoros se adaptaron a la altura y a la aridez del ambiente, desarrollando unas almohadillas en sus patas que minimizan el efecto erosivo del pisoteo. Además, poseen una dentadura que corta los pastos sin arrancarlos, lo que también facilita su rebrote. </p> 
          <p>La Reserva de Biósfera se encuentra en el extremo Noroeste de la provincia de San Juan, abarcando
            aproximadamente al 45% del departamento de Iglesia. Limita al Oeste con la Cordillera de Los Andes (límite
            con
            Chile); al Norte con la provincia de La Rioja; al Este con el Río Blanco hasta la confl uencia con el Río de
            La
            Palca; y al Sur con la línea imaginaria que resulta de unir el Paso de las Tórtolas (en el límite
            internacional
            argentino - chileno) con el punto de confl uencia entre el Río de La Palca y el Río Blanco. Su centro
            geográfi
            co aproximado está a 29° 10’ de latitud Sur y 69° 20’ de longitud Oeste.</p>
            <p>San Guillermo posee un alto número de endemismos; es decir, especies con una distribución restringida a un área determinada. Las especies endémicas documentadas hasta el momento incluyen dos reptiles: el Chelco de San Guillermo (Liolaemus eleodori) y el Cola de Piche de San Guillermo (Phymaturus punae). Ello también se da con los mamíferos: el Ratón de la Vega (Neotomys ebriosus) y la Rata Chinchilla (Abrocoma cinerea).También poseería una de las poblaciones más australes del amenazado Gato Andino (Oreailurus jacobita), usualmente llamado Gato Onza. La presencia de este felino fue documentada por WCS (Wildlife Conservation Society) en 2004. Los esfuerzos de investigación de WCS también proporcionaron los 
            primeros registros para el área de otras especies de carnívoros: el Gato de Pajonal (Oncifelis colocolo), el Zorro Colorado (Lycalopex culpeus) y el Zorro Gris Chico (Lycalopex gimnocercus). Incluso podemos encontrar al Puma (Puma concolor), el carnívoro de mayor tamaño presente en el área y visible a pleno día. Hay otras especies de mamíferos con un registro incompleto, incluyendo a la altamente amenazada Chinchilla de Cola Corta (Chinchilla brevicaudata). </p>
            <p>Se registra una gran variedad de roedores, como la Vizcacha de la Sierra o Chinchillón (Lagidium viscacia) - presentes en quebradas y lugares abiertos - y el oculto Tuco-Tuco (Ctenomys sp.), el que suele refugiarse en cuevas o galenas de piedras para protegerse de las inclemencias del clima.</p>
            <p>San Guillermo registra una gran diversidad de aves: El Suri Cordillerano (Pterocnemia tarapacensis), especie amenazada a nivel nacional, que en el área recorre en grupos las pampas de altura; la Quiula Puneña o Keu Andino (Tinamotis penlandii), similar a la Martineta Común, pero sin copete y también amenzada; el Flamenco Austral (Phoenicopterus chilensis); el Matamico Andino (Phalcoboenus megalopterus); la Guayata o Piuquén (Chloephaga melanoptera); el Cóndor Andino (Vultur gryphus); el Carancho (Caracara plancus); la Gallareta Cornuda (Fulica cornuta); el Batitú (Bartramia longicauda), de la familia de los playeritos; la Agachona de Collar o Puco-Puco de Altura (Thinocorus orbignyianus); la Agachona Grande (Attagis gayi); la Palomita Dorada o de la Sierra (Metriopelia aymara); el Piojito Gris (Serpophaga nigricans) y el Comesebo Andino o Boquense (Prygilus gayi). Los peces nativos están representados por una única especie, el Pique (Hatchería macraeí), una especie de pez gato que alcanza 21 cm y es endémica de los ríos andinos de Argentina.</p>
          
          
            </div>

      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);

}

function floraSanGuillermo(){
  activarFlora();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="./src/img/San-Guillermo/Flora-1.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/San-Guillermo/Flora-2.png" class="imagenes d-block w-100" alt="...">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 


      $("#descripcion2").html("");


      htmlHeader = 
      `
      <div class="descripcion2">
          <p>UNIDADES FITOGEOGRÁFICAS</p>
          <p>Hay tres provincias fitogeográficas presentes en San Guillermo: Monte, Puna y Altoandina, con transiciones entre ellas que varían según la exposición y la pendiente (de Este a Oeste, se asciende de los 1.700 m a más de 5.000 m de altura). El Cardonal, si bien no se encuentra incluido en la misma área protegida, aparece en las cercanías en laderas de exposición Norte ni bien se comienza el ascenso por la vertiente oriental de los Andes.            </p>
          <p>La ecorregión ALTOANDINA se presenta por sobre los 4000 msnm. y hasta los 4400 metros (límite de vegetación en el área). Se caracteriza por presentar vegetación en cojines. Allí domina el género Adesmias (Adesmia sp.) y elementos acompañantes como el Coirón (Stipa frígida), entre otros. </p> 
          <p>La ecorregión PUNEÑA se presenta en la RBSG con variadas fi sonomías y es el piso más importante en cuanto a extensión, ubicándose entre los 3.000 y 3.800 msnm. También hay un ecotono de piso superior al Altoandino, variable en extensión según topografía, entre 3.800 y 4.200 msnm. </p>
          <p>Entre las comunidades vegetales puneñas, se han identifi cado las siguientes:</p>
          <p>a) Matorrales de Ajenjo (Artemisia sp), Lycium (Lycium chañar), Tola (Fabiana punensis), Jarilla (Larrea divaricata), Pinchagua (Lycium fuscum) y Adesmia pinifolia</p>
          <p>b) Monte Negro (Trycicla spinosa) </p>
          <p>c) Pastizales de Coirón (Stipa chrysophylla var. chrysophylla, de Stipa speciosa var. Abscondita) </p>
          <p>La ecorregión del MONTE se encuentra representada en la zona Sur de la RBSG hasta los 2800 msnm. Se caracteriza por presentar matorrales abiertos, particularmente en fondos de valles. Las especies propias que identifi can este ambiente en el área son varias: Algarrobo Dulce (Prosopis 
            fl exuosa var. Depressa), Jarilla (Larrea divaricata), Retamo 
            (Bulnesia retama), etc.</p>
          <p>VEGETACIÓN DE VEGAS</p>
          <p>Las vegas son los ambientes de mayor importancia para la biodiversidad. El porcentaje de cobertura vegetal y la biomasa aérea es signifi cativamente mayor que en otros ambientes de altura. Así también, las vegas presentan el mayor número de especies vegetales, considerando que se encuentran tanto en el piso puneño como en el altoandino. Así, posee diferentes cinturones de vegetación de acuerdo al grado de saturación de los suelos. </p>
          <p>Se identifican cinco comunidades de vegetación:</p>
          <p>1- Juncus arcticus</p>
          <p>2- Carex</p>
          <p>3- Potasia clandestina</p>
          <p>4- Juncus balticus</p>
          <p>5- Potamogeton pectinatus</p>
          <p>6- Aquellas de base granítica, con presencia de Urtica buchtienii</p>
          <p>Las vegas tienen una importancia particular para la avifauna. También tiene gran relevancia como hábitat para el único representante de los anfibios conocido en el área: el Sapo Andino (Bufo spinolosus), razón por la cual esta especie podría ser un buen indicador de contaminación de las aguas. Por otro lado, numerosos estudios han confi rmado la dependencia de los camélidos respecto de estos ambientes. La presencia de la juncácea Oxychloe sp. como segundo componente relevante en la dieta de la vicuña y presente únicamente en las vegas de altura, podría indicar que esta planta tiene un papel destacado en la distribución de la especie. </p>
          <p>Para los predadores, las vegas también constituyen un ambiente de preferencia, ya que son sitios ideales para la caza - respectivamente - de camélidos. </p>
          <p>Las juncáceas son escondites efi caces para el Puma (Puma concolor) al momento de acechar sus presas. Ya que los camélidos representan el principal ítem en la dieta del puma en San Guillermo, puede inferirse que los ambientes de vega resultan vitales en la dinámica predatoria de estos carnívoros.</p>
          <p>Hay que destacar también que el área posee un antiguo uso ganadero que se remonta a los períodos prehispánico y fi nales del Siglo XVI, el cual ha tenido una infl uencia directa en la introducción de especies exóticas como el Burro (Equus africanus asinus), el Caballo (Equus ferus caballus) y la Oveja (Ovis orientalis aries).</p>

          </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);


}
function geoSanGuillermo(){
  activarGeo();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d890098.440705852!2d-69.3430181219542!3d-29.371840026116722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x969b420fe75eac21%3A0x18f6eae9c8b423f3!2sParque%20Nacional%20San%20Guillermo!5e0!3m2!1ses!2sar!4v1640614166409!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">
          <p>Coordenadas • 29°06′S 69°12′O</p>
          <p>Ecorregiones • Monte de sierras y bolsones, Puna, y Altos Andes</p> 
          <p>La Reserva de Biósfera se encuentra en el extremo Noroeste de la provincia de San Juan, abarcando
            aproximadamente al 45% del departamento de Iglesia. Limita al Oeste con la Cordillera de Los Andes (límite
            con
            Chile); al Norte con la provincia de La Rioja; al Este con el Río Blanco hasta la confl uencia con el Río de
            La
            Palca; y al Sur con la línea imaginaria que resulta de unir el Paso de las Tórtolas (en el límite
            internacional
            argentino - chileno) con el punto de confl uencia entre el Río de La Palca y el Río Blanco. Su centro
            geográfi
            co aproximado está a 29° 10’ de latitud Sur y 69° 20’ de longitud Oeste.</p>
           </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);
}
////////////////////////////////////////////////   Parque Nacional El Leoncito  /////////////////////////////////////////////

function climaLeoncito(){
  activarClima();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide img-carousel" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
          <img  src="./src/img/Leoncito/Clima-1.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img  src="./src/img/Leoncito/Clima-2.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img  src="./src/img/Leoncito/Clima-3.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img  src="./src/img/Leoncito/Clima-4.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img  src="./src/img/Leoncito/Clima-5.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img  src="./src/img/Leoncito/Clima-6.png" class="imagenes d-block w-100" alt="...">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">

          <p>Se trata de una región árida con escasas precipitaciones (en forma de nieve en los sectores más altos) y vientos secos. Se registra gran amplitud térmica: durante el día, se producen temperaturas altas en verano y muy bajas en las noches de invierno.</p>
          <p>Abarca un interesante gradiente ambiental que va desde el valle de Calingasta, a unos 1.900 metros de altura, hasta el fi lo de El Tontal. Las precipitaciones anuales del área varían entre los 100 y los 400 mm.; las temperaturas medias son de 22°C en las zonas bajas y de 10°C en las de altura.
          </p>
          <p>El clima varía de acuerdo a la altura, llevando a distinguir tres tipos:</p>
          <p>• Altoandino: Clima frío, con nieves permanentes.</p>
          <p>• Monte: Subtropical-seco.</p>
          <p>• Puna: Clima frío y seco, con gran amplitud térmica</p>
          
           </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);


      
}
function faunaLeoncito(){
  activarFauna();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <div id="floraCarousel" class="carousel slide img-carousel"  data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active" >
                <img  src="./src/img/Leoncito/Fauna-1.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Fauna-2.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Fauna-3.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Fauna-4.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Fauna-5.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Fauna-6.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Fauna-7.png" class="d-block w-100 imagenes" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
          
  
  
  
  
  
  
          $("#descripcion2").html("");
          htmlHeader = 
          `
          <div class="descripcion2">
              <p>Entre los mamíferos del área, se confirmó la presencia de 14 especies autóctonas y 1 exótica, de presencia probable. Entre ellos se encuentra el Puma (Puma concolor), cuya presa más grande es el Guanaco (Lama guanicoe). También están presentes el Zorro Colorado (Lycalopex culpeus) y Zorro Gris  Chico (Lycalopex gymnocercus). Entre los roedores, se destacan el Cuis Chico (Microcavia australis), el Tuco-Tuco Amarillo (Ctenomys fulvus), el Cuis Moro (Galea musteloides), la Laucha Colilarga Baya o Laucha Sedosa Pampeana (Eligmodontia typus), el Chinchillón Común (Lagidium viscacia), el Piche Llorón (Chaetophractus vellerosus), la Comadrejita Común (Thylamys elegans), la Rata Chinchilla (Abrocoma cinerea), el Ratón Andino (Akodon 
                andinus) y el Zorrino Común (Conepatus chinga).</p>
              <p>En el área se encuentran presentes 99 especies de aves y - de las mismas - 7 son nidifi cantes. Entre ellas se encuentra el Suri Cordillerano (Rhea pennata), conocido localmente como “avestruz” o “churi”, el ave emblemática del Parque. También están la Guayata o Piuquén (Chloephaga melanoptera), el Cóndor Andino (Vultur gryphus), el Aguilucho Alas Largas (Buteo albicaudatus), el Matamico Andino (Phalcoboenus megalopterus), el Halcón Peregrino (Falco peregrinus), el Gavilán Mixto (Parabuteo unicinctus); el Águila Mora (Geranoaetus melanoleucus); la Agachona Grande (Attagis gayi), la Palomita Dorada (Metriopelia aymara), la Catita Serrana Grande (Bolborhynchus aymara) y la Catita Serrana Chica (Bolborhynchus aurifrons). El listado de aves presentes en el área es realmente extenso: el Ñacurutú (Bubo virginianus), la Caminera Común (Geositta cunicularia), la Bandurrita Pico Recto (Upucerthia rufi cada), el Coludito Canela (Leptasthenura fuliginiceps), la Dormilona Chica (Muscisaxicola maculirostis), la Golondrina Barranquera (Notiochelidon cyanoleuca), el Naranjero (Thraupis bonariensis), el Chingolo (Zonotrichia capensis), la Loica común (Sturnella loyca); el Yal Andino (Melanodera xanthogramma) y el Cabecitanegra Común (Cardellius magellanica), entre otras.</p> 
              <p> Se conocen hasta ahora 11 especies de reptiles de presencia comprobada y 3 de presencia dudosa. Éstos suelen buscar zonas donde puedan asolearse lo más posible. Existen varias especies de lagartijas, como el Cola de Piche (Phymaturus sp.), la Lagartija Pintas Amarillas o Precordillerana (Liolaemus ruibali), la Lagartija de Uspallata (Liolaemus uspallatensis), el Gecko Salamanca (Homonota fasciata) y el Geko Andino (H. andicolas). Entre los ofidios, se encuentran la Culebra Jarillera o Conejera (Philodryas trilineatus) y la Yarará Ñata (Bothrops ammodytoides).</p>
              
              </div>
  
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
  
}
function floraLeoncito(){
  activarFlora();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <div id="floraCarousel" class="carousel slide img-carousel" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img  src="./src/img/Leoncito/Flora-1.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Flora-2.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Flora-3.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Flora-4.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Flora-5.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Flora-6.png" class="d-block w-100 imagenes" alt="...">
              </div>
              <div class="carousel-item">
                <img  src="./src/img/Leoncito/Flora-7.png" class="d-block w-100 imagenes" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
  
          $("#descripcion2").html("");
  
  
          htmlHeader = 
          `
          <div class="descripcion2">
              <p>De las zonas bajas del valle, donde hay verdaderos oasis y se utiliza la tierra para cultivos, se pasa al desierto casi sin transición alguna. Aunque las condiciones son sumamente adversas, los seres vivos adquirieron mecanismos complejos de adaptación para poder sobrevivir y desarrollarse a más de 3.000 metros de altura.
              En el área se encuentran representadas las provincias fitogeográficas del Monte, Puna y Altoandina, en un rango altitudinal que va desde los 2.900 msnm. hasta los 4.300 msnm. Hasta los 3.200 msnm., la flora predominante está compuesta por matorrales abiertos que - a medida que aumenta la altitud - van tornándose más pequeños. Luego podemos encontrar al pastizal de Coirón (Stipa sp.).</p>
              <p>En la zona de Monte predominan los Retamos (Bulnesia retama) - cuyos conjuntos se encuentran bastante distanciados entre sí, dejando la mayor parte del terreno descubierto- y los jarillales (como los que se pueden encontrar cerca del Observatorio). El Retamo fue intensamente cortado durante décadas para la obtención de cera; en la actualidad, se lo emplea en menor escala como leña, pudiéndose comprobar su lenta recuperación.</p>
              <p>Las Jarillas (Larrea divaricata, L. nitida, etc.) se encuentran en ambientes salobres junto a gramíneas como el Pelo de chancho (del género Distichlis) y matas de Senecio fi laginoides. El piso puneño presenta diferentes especies o asociaciones, conformadas por Chañar (Lycium chanar), Ajenjo (Artemisia absinthium), Acerillo o Cuerno de cabra (Adesmia pinifolia) y Coirón (Stipa vaginata, S. scirpea y S. speciosa). En el piso altoandino se destaca la presencia de Coirón blanco (Poa huecu), Coirón (Stipa ibari), Iro (Festuca desvauxii), Taihuana (Mulinum echegarayi) y Tramontana (Junellia unifl ora), entre otras.</p>
              <p>Sobre algunos faldeos de las sierras son comunes los cojines de Maihueniopsis glomerata, un tipo de cactus conocido en la zona con el nombre de Leoncito, el que obviamente inspiró el nombre tanto de la estancia como del actual Parque. No obstante, hay versiones encontradas respecto a este tema. Otro elemento típico de este paisaje lo constituyen los ejemplares de Lobivia formosa, un cactus de forma esférica que se ubica sobre las laderas rocosas. El mismo es robusto, de aproximadamente un metro de altura, con densas y largas espinas, color blanquecino. Otras cactáceas presentes son la Pterocactus reticulatus y la Puna clavarioides. Entre las malváceas están la Nototriche sp. y Sphaeralcea philippiana. En los suelos salitrosos, se desarrollan las halófi las (plantas adaptadas a suelos salinos).Se han realizado relevamientos fl orísticos en los sectores húmedos de la zona, los que indican la presencia de 157 especies diferentes para estos ambientes del Parque Nacional (vegas, arroyos, oasis irrigado y depresiones naturales). Del total, unas 32 especies son exóticas. Hay elementos de las áreas húmedas que corresponden al Monte hasta los 2.400 msnm. A partir de allí, y hasta los 3.300 msnm., se combinan los elementos de Prepuna y Puna.</p>
              <p>En las vegas o ciénagas, se presentan otras especies asociadas a los pequeños cuerpos y cursos de agua de alta montaña. El Berro (Nasturtium offi cinale), el Llantén (Plantago lanceolata), diversos tipos de Juncos (Juncus sp.) y una gran variedad de plantas prosperan en estos sectores. En los bordes de la vega son notables las Yaretas (Azorella compacta), arbustos achaparrados de follaje muy compacto, que forman grandes placas de bordes circulares y de baja altura. Junto al arroyo Leoncito - el único curso permanente de todo ese lado del Tontal - hay matorrales de Pájaro bobo (Tessaria absinthioides), un arbusto que crece en densos parches. </p>
              <p>Estos sectores son los hábitats de varias especies de plantas y animales endémicos, constituyendo al Parque Nacional ‘El Leoncito’ como la única área natural protegida donde se los ampara.</p>
              </div>
          
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
  
  
}
function geoLeoncito(){
  activarGeo();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3391.012446414083!2d-69.3761396842399!3d-31.797410721967886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9686ebfb76fc6fbb%3A0x927d0dcaa1badda8!2sParque%20Nacional%20El%20Leoncito!5e0!3m2!1ses!2sar!4v1642775394327!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
          $("#descripcion2").html("");
          htmlHeader = 
          `
          <div class="descripcion2">
          <p>Coordenadas	•	31° 18' S y 69° 30' O</p>
          <p>Ecorregiones	•	Altos Andes - Monte de Sierras y Bolsones</p> 
          <p>El Parque Nacional ‘El Leoncito’ se encuentra ubicado al Sudoeste de la Provincia de San Juan, en Calingasta, sobre los faldeos occidentales de las Sierras del Tontal (Precordillera Andina). Latitud Sur: 31° 18' - Longitud Oeste: 69° 30'. </p> 
          <p>ACCESOS VIALES</p> 
          <p>La Ruta Provincial 412, que pasa por la localidad de Barreal, permite acceder al Parque desde el Norte. Desde Uspallata (Mendoza), la Ruta Provincial 39 - continuación mendocina de la Ruta 412 - brinda un ingreso desde el Sur. </p>
          <p>Desde la Ruta 412 hacia el Este, a unos 20 Km. al Sur de Barreal, existe un camino de 12 Km. que conduce a la recepción del Parque Nacional.</p>
          <p>DISTANCIAS AL PARQUE NACIONAL EL LEONCITO</p>
          <p>• San Juan (Capital): 245 Km.</p>
          <p>• Barreal: 35 Km. </p>
          <p>• Mendoza (Capital): 200 Km.</p>
          <p>• Uspallata: 90 Km.</p>
          </div>
          
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
}

////////////////////////////////////////////////////////   GUANACACHE   ///////////////////////////////////////////////////////////////

function climaGuanacache(){
  activarClima();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
        <div class="carousel-item active">
        <img src="./src/img/Guanacache/Clima-1.png" class="imagenes d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="./src/img/Guanacache/Clima-2.png" class="imagenes d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="./src/img/Guanacache/Clima-3.png" class="imagenes d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="./src/img/Guanacache/Clima-4.png" class="imagenes d-block w-100" alt="...">
      </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">

          <p>El clima predominante en la zona de las Lagunas de Huanacache es del tipo árido, acentuado por características desérticas y con bajos valores pluviales medios anuales. Hay algo más de un 50% de humedad relativa y una temperatura media anual que se encuentra en torno a los 18º C. Los vientos predominantes en el sector Sur de la provincia pertenecen al cuadrante Sur-Sureste.
           No obstante, cabe destacar la presencia frecuente del viento Zonda y de un viento arenoso bastante común en la zona del Encón, proveniente del cuadrante Este.</p>
          </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);


      
}

function faunaGuanacache(){
  activarFauna();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
        <div class="carousel-item active">
        <img src="./src/img/Guanacache/Fauna-1.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Guanacache/Fauna-2.png" class="imagenes d-block w-100" alt="hola">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Guanacache/Fauna-3.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Guanacache/Fauna-4.png" class="imagenes d-block w-100" alt="hola">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Guanacache/Fauna-5.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Guanacache/Fauna-6.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
          <img src="./src/img/Guanacache/Fauna-7.png" class="imagenes d-block w-100" alt="...">
        </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      






      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">
          <p>La fauna general del sitio presenta elementos típicos de la provincia biogeografi ca del monte, mientras que hacia el Sur se ensamblan elementos patagónicos. 
          En consecuencia, la fauna que se ha podido identifi car está compuesta por distintas especies, de las cuales las más representativas son:</p>
          <p>• Felinos, como el Gato Montes (Oncifelis geoff royi), el Gato del Pajonal (Lynchailurus pajeros), el Gato Moro o Eyra (Herpailurus yaguarondi) y el Puma (Puma concolor) que se encuentran en toda la zona.</p> 
          <p> • Roedores, entre los que se destacan la Mara o Liebre Criolla (Dolichotis patagonum), el Coipo o Nutria (Myocastor coypus) y los Cuises (Microcavia australis).</p>
          <p>• Las aves son el grupo más abundante, siendo uno de los componentes faunísticos principales que justifi can la declaración del área como Sitio Ramsar. Algunas de estas son de grandes dimensiones y corredoras, como el Ñandú (Rhea americana). Hay varias especies de Perdices(Nothoprocta sp.), y Martinetas (Eudromia elegans). Las rapaces están representadas por diferentes falcónidos y águilas, entre las que se destaca el Águila Coronada (Harpyhaliaetus coronatus), recientemente declarada como especie en peligro de extinción. Se observan también importantes bandadas de Loros y Catas (Psitacidos), sobresaliendo por su población la Catita Verde Común (Myopsitta monachus) cuyos nidos son cada vez más abundantes. También se pueden observar Carpinteros (Colaptes melanolaimus) y trepadores como los Chincheros (Lepidocolaptes angustirostris). Hay una gran diversidad de paseriformes del monte: Chingolos (Zonotrichia capensis australis), Jilgueros (Carduelis magellanica), 
          Boquense (Phrygilus gayi), Corbatitas (Sporophila collaris) y Diuca (Diuca diuca) entre otras. En los bañados de El Encón y del Bermejo, hay importantes zonas de nidifi cación de aves de ambientes acuáticos, especialmente de Flamencos (Phoenicopterus chilensis) y de varios tipos de Garzas y Patos.</p>
          <p>• Numerosos Reptiles (Lagartos y lagartijas) también Ofi dios como la Lampalagua (Boa constrictor occidentalis) y algunas Culebras. 
          También se suele observar la Tortuga Terrestre (Geochelone chilensis) que cada vez es más escasa debido a su comercio y tráfico ilegal.</p>
          <p>• Los invertebrados tiene numerosos representantes en la zona debido a la humedad presente en estos ambientes. 
          </p>
          <p>• Entre los peces hay una especie introducida e invasiva, la Carpa (Cyprinus carpio), que ha desplazado a la especies nativas</p>
          
          </div>

      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);

}

function floraGuanacache(){
  activarFlora();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="./src/img/Guanacache/Flora-1.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Guanacache/Flora-2.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Guanacache/Flora-3.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Guanacache/Flora-4.png" class="imagenes d-block w-100" alt="...">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 


      $("#descripcion2").html("");


      htmlHeader = 
      `
      <div class="descripcion2">
          <p>El Sitio Ramsar conforma el humedal más extenso de la región cuyana. 
          El mismo contiene una gran variedad de hábitats que albergan a una importante diversidad de especies animales y vegetales, constituyéndose en un patrimonio de gran valor para su conservación.</p>
          <p>La flora se caracteriza por la presencia de un extendido estrato herbáceo, en el que se observan las comunidades de Junquillar y la del Pastizal seco e inundado, además de otras especies rastreras. Se presenta también un estrato arbóreo, rebrote de un bosque originario que fuera explotado intensamente en tiempos pasados. En la actualidad, se puede apreciar la presencia de importantes islas de bosque nativo, algarrobales y chañarales, los que se erigen como principales componentes de este estrato. 
          En menor medida, se observa una discreta presencia del Quebracho Blanco en la región correspondiente a las laderas de las Sierras de Guayaguás. El estrato de mayor cobertura está constituido por especies arbustivas, los que conforman Retamales, Jarillales, Zampales, Chilcales y Lamarales. 
          En general, su distribución responde al relieve y a las características del suelo. La vegetación palustre está representada por Juncáceas y Ciperaceas de diferentes especies. 
          Se encuentran además especies introducidas (como el tamarindo), las que se han invadido las márgenes del Rio San Juan.</p>
          </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);


}
function geoGuanacache(){
  activarGeo();
  $("#prueba").html("");
      let htmlHeader = 
      `
      <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6745.386040398886!2d-67.35134417816096!3d-32.29323039746557!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x76403ff72438379f!2zMzLCsDE3JzM5LjciUyA2N8KwMjAnNTIuNiJX!5e0!3m2!1ses!2sar!4v1642426321612!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

      `
      let htmlFin = ``
      $("#prueba").html(htmlHeader+htmlFin); 

      $("#descripcion2").html("");
      htmlHeader = 
      `
      <div class="descripcion2">
    <p>Coordenadas	•	32° 15' S y 67° 32' O</p>
        <p>Ecorregiones	•	Altos Andes - Monte de Sierras y Bolsones</p> 
        <p>El sitio Ramsar en San Juan se encuentra emplazado al Sur del territorio de la provincia. Su área local se reparte entre los departamentos de Sarmiento y 25 de Mayo. Los límites - establecidos en la ficha técnica de Ramsar para el área de jurisdicción sanjuanina - son los siguientes:</p> 
        <p>- Al Oeste, la Ruta Nacional Nº 40. </p> 
        <p>- Al Sur, el límite interprovincial San Juan - Mendoza y el cauce del Río San Juan.</p>
        <p>- Al Norte, el paralelo 32 S</p>
        <p>- - Al Este, las divisorias de aguas de las Sierras de Guayaguas y el límite provincial con San Luis.</p>
        <p>El sitio Ramsar San Juan comienza a sólo dos kilómetros al Sur de la Villa de Media Agua, en Sarmiento, y a unos 17 kilómetros al Sur - Este de Villa Santa Rosa, cabecera del departamento 25 de Mayo.</p>
        <p>ACCESOS</p>
        <p>El Sitio Ramsar Lagunas de Huanacache es accesible desde la ciudad de San Juan por distintas rutas. Uno de los accesos es la Ruta Nacional Nº 40, ben dirección a Mendoza. El Sitio Ramsar comienza a unos 2 Km. al Sur de Villa Media Agua. 
        Desde allí hasta el limite provincial, se pueden apreciar las características de un importante sector productivo que se destaca por la calidad del melón y la uva. 
        Cercano al límite interprovincial, se puede observar el severo proceso de desertifi cación que afecta a gran parte del Sitio Ramsar cuyano, sector que en siglos pasados se encontraba cubierto de aguas.</p>
        <p>El otro acceso importante es la Ruta Nacional Nº 20, que atraviesa al sitio con dirección Sur-Este vinculando a la provincia con el centro del país. Sobre esta ruta se encuentra El Encón, único poblado urbanizado, ubicado dentro del Sitio Ramsar en la jurisdicción de San Juan. 
        Próximo a este pueblo se pueden observar y disfrutar las bellezas naturales de los médanos cercanos y del bañado del Río San Juan, visitado estacionalmente por importantes poblaciones de fl amencos y otras aves migratorias. 
        Siguiendo rumbo Sur-Este previo a la localidad de Las Trancas, se observan los Bañados del Bermejo, cuyo nivel de agua - dependiendo del caudal del Río San Juan o de las lluvias estacionales - llega a veces hasta la ruta. Existen otras formas de llegar, aunque con menor accesibilidad debido a las condiciones precarias de la red de caminos y de las huellas departamentales. Entre ambas rutas nacionales se encuentra la Ruta Provincial Nº 319, la que pasa por la Comunidad Sawa, Punta del Agua, Colonia San Antonio y Media Agua.</p>
        
        </div>
      
      `
      htmlFin = `` 
      $("#descripcion2").html(htmlHeader+htmlFin);
}
  ////////////////////////////////////////////////////////   ISCHIGUALASTO   ///////////////////////////////////////////////////////////////

  function climaIschigualasto(){
    activarClima();
    $("#prueba").html("");
        let htmlHeader = 
        `
        <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="./src/img/Ischigualasto/Clima-1.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Clima-2.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Clima-3.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Clima-4.png" class="imagenes d-block w-100" alt="...">
            </div>

          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>

        `
        let htmlFin = ``
        $("#prueba").html(htmlHeader+htmlFin); 

        $("#descripcion2").html("");
        htmlHeader = 
        `
        <div class="descripcion2">

         
         


            <p>El clima y el relieve son los factores ambientales principales que condicionan a la distribución de las unidades ecológicas y de la biodiversidad.</p>
            <p>El clima local es el resultado de numerosos y complejos factores. De ellos, cabe citar a la distancia a las fuentes oceánicas, la orografía, la ubicación y el desplazamiento de centros de alta y baja presión, la altura con respecto al nivel del mar, etc. Se caracteriza principalmente por ser un ámbito áridodesértico, con grandes amplitudes térmicas diurnas-nocturnas y anuales (verano-invierno). Se registra una elevada heliofanía e insolación con una importante transparencia atmosférica y escasa humedad. Se presentan lluvias - exclusivamente estivales - en el sector deprimido entre Precordillera y Cordillera Frontal. Hay precipitaciones sólidas invernales (principalmente en forma de nieve) en la zona cordillerana, con una muy baja frecuencia 
            media de días con precipitaciones (sean éstas líquidas o sólidas). Las temperaturas son bajas la mayor parte del año, en particular para los relieves elevados.</p> 
            
             </div>
        
        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);


        
  }

  function faunaIschigualasto(){
    activarFauna();
    $("#prueba").html("");
        let htmlHeader = 
        `
        <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="./src/img/Ischigualasto/Fauna-1.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Fauna-2.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Fauna-3.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Fauna-4.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Fauna-5.png" class="imagenes d-block w-100" alt="...">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>

        `
        let htmlFin = ``
        $("#prueba").html(htmlHeader+htmlFin); 

        






        $("#descripcion2").html("");
        htmlHeader = 
        `
        <div class="descripcion2">
            <p>Al igual que la flora, la fauna presenta adaptaciones - de conducta y de fisonomía - a las condiciones climáticas, ejemplificadas en los hábitos cavícolas. 
            Entre las aves se destacan el Ñandú (Rhea americana), el Canastero Castaño (Asthenes steinbachi), el Canastero Rojizo (Asthenes dorbignyi) y la Martineta Común (Eudromia elegans). Entre las rapaces están el Cóndor Andino (Vultur gryphus) –que nidifica en el área-, el Halcón Peregrino (Falco peregrinus) y el Halconcito Gris (Spiziapteryx circumcinctus). 
            También pueden avistarse al Pitotoy Grande (Tringa melanoleuca), la Catita Serrana Grande (Bolborhynchus aymara), el Calacante Común (Aratinga acuticaudata), el Picafl or Común (Chlorostilbon aureoventris), el Carpintero del Cardón (Melanerpes cactorum), el Gallito Arena (Teledromas fuscus), la Calandria Mora (Mimus patagonicus), el Zorzal Chiguanco (Turdus chiguanco), la Monterita Canela (Poospiza ornata), el Jilguero Oliváceo (Sicalis olivascens), etc.</p>
            <p>Entre los mamíferos se han relevado 14 especies, de las cuales se considera que un 46% presenta algún problema de conservación actual o potencial. El 24% merece atención especial y el 22% requiere de la máxima prioridad de conservación. 
            Se encuentran el Piche Llorón (Chaetophractus vellerosus), el Piche Patagónico (Zaedyus pichiy), el Puma (Puma concolor), el Guanaco (Lama guanicoe), la Rata Cola Peluda o Pincel (Octomys mimax), el Pericote Panza Gris (Phyllotis darwin), el Cuis Chico (Microcavia australis), la Vizcacha (Lagostomus maximus), la Liebre Europea (Lepus europeus) -especie exótica introducida-, la Mara o Liebre Criolla (Dolichotis patagonum), y el Chinchillón Común (Lagidium viscacia), entre otras./p> 
            <p>El listado registrado de reptiles llega a 18 especies, de las cuales el 55% presenta problemas de conservación actual o potencial. 
            El 39% de éstas merecen ser consideradas para una categoría especial de atención, y el 16% requiere máxima prioridad de conservación. 
            Entre estas especies podemos encontrar a la Tortuga Terrestre Común (Chelonoidis chilensis), especie que - a nivel nacional - se encuentra en gran retroceso numérico debido a la captura indiscriminada para la venta ilegal como mascota y la destrucción de su ambiente natural. 
            Hay varias especies exclusivas, como el Chelco Arenero (Leiosaurus catamarcensis), el Lagarto Fajado (Pristidactylus fasciatus), la Lagartija Cuyana (Liolaemus cuyanus), la Lagartija Riojana (Liolaemus riojanus), la Lagartija Listas Amarillas (Liolaemus darwii), la Lagartija Salinera (Liolaemus anomalus), la Lagartija del Desierto (Liolaemus pseudoanomalus) –estás dos últimas amenazadas-, el Geko Norteño (Homonota borelli), el Geko de Monte (Homonota underwoodi) y la Salamanca (Homonota fasciata). </p>
            <p>Además están el Ututu Coludo (Cnemidophorus longicaudus), el Teyú Chaqueño (Teius teyou), la Culebra Jarillera o Conejera (Philodryas trilineatus), la Falsa Yarará (Waglerophis merremii), la Víbora de Coral (Micrurus pyrrhocryptus), la Yarará Chica (Bothrops neuwiedi diporus) y la Yarará Ñata (Bothrops amoditoides). 
            Varias de las mismas son exclusivas del área o del país.</p>
            <p>Sólo en pocos sitios - dónde hay reservorios permanentes de agua - podemos encontrar anfi bios, tales como el Sapo Común (Rhinella arenarum), el Sapo Andino (Rhinella spinulosus) y algunas ranas, tales como la Pleurodema nebulosa y el Escuercito (Odontophrynus occidentalis). 
            Varias de las mismas resisten a la temporada de sequía enterradas en la arena, a la espera de lluvia.</p>
            
            </div>

        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);

}
  
  function floraIschigualasto(){
    activarFlora();
    $("#prueba").html("");
        let htmlHeader = 
        `
        <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="./src/img/Ischigualasto/Flora-4.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Flora-1.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item ">
              <img src="./src/img/Ischigualasto/Flora-2.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Ischigualasto/Flora-3.png" class="imagenes d-block w-100" alt="...">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>

        `
        let htmlFin = ``
        $("#prueba").html(htmlHeader+htmlFin); 


        $("#descripcion2").html("");


        htmlHeader = 
        `
        <div class="descripcion2">
            <p>Domina la vegetación adaptada a condiciones de sequía (xerófita), encontrándose obviamente limitada por la baja disponibilidad de agua. Algunas de las estrategias de las mismas están relacionadas a la reducción del tamaño de sus hojas, y el desarrollo de espinas y raíces modifi cadas para aumentar así su capacidad de captar agua. A este fin también contribuye la presencia de resinas que cubren las hojas y los tallos, impidiendo una gran evaporación en algunas especies.
            </p>
            <p>Entre la flora se pueden mencionar al Retamo (Bulnesia retama), arbusto que presenta un tronco ramifi cado casi desde la base, con ramas gruesas, lisas, con extremos delgados y erectos. 
            Cuando la planta aun es jóven posee gran flexibilidad, pero al pasar del tiempo, las ramas se tornan quebradizas y sus extremos suelen secarse en épocas desfavorable. 
            Posee hojas pequeñas de entre 4 y 6 mm. que se caen al poco tiempo. La Zampa (Atriplex sp.) es un arbusto de hasta 1,5 m de altura, copa densa, tallo estriado y quebradizo, y posee hojas pequeñas de coloración cenicienta.</p>
            <p>Se pueden encontrar árboles como el Chañar (Geoffroea decorticans) de hasta 4 metros de alto, con ramas espinosas y un tronco amarillo-parduzco que exfolia en bandas, dejando áreas verdes y hojas pequeñas oblongas. 
            También hay algunas especies de Algarrobo (Prosopis spp.), de hasta 8 metros de alto, con ramaje abierto, espinoso y follaje escaso, caduco en invierno, con copa algo aparasolada y tronco bajo.</p> 
            <p>Además están presentes la Jarilla (Larrea sp.), la Cortadera (Cortaderia selloana), la Chilca (Baccharis salicifolia), la Verdolaga o Flor de un Día (Portulaca grandiflora) - presente sólo en verano, época de abundancia - y el Tintitaco (Prosopis torquata). 
            Hay algunas cactáceas como el Cardón (Trichocereus terschesckii) y la Echinopsis leucantha, un cactus de 60 cm. por 20 cm. de diámetro, verde oscuro y flores muy vistosas de color blanco o rosado, las que se abren al ponerse el sol y se cierran en las primeras horas del amanecer. 
            El Parque también dispone de añosos bosquesillos de Chica (Ramorinoa girolae), únicos en la región.</p>
            </div>
        
        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);


  }
  function geoIschigualasto(){
    activarGeo();
    $("#prueba").html("");
        let htmlHeader = 
        `
        <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d890098.440705852!2d-69.3430181219542!3d-29.371840026116722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x969b420fe75eac21%3A0x18f6eae9c8b423f3!2sParque%20Nacional%20San%20Guillermo!5e0!3m2!1ses!2sar!4v1640614166409!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

        `
        let htmlFin = ``
        $("#prueba").html(htmlHeader+htmlFin); 

        $("#descripcion2").html("");
        htmlHeader = 
        `
        <div class="descripcion2">
            <p>Coordenadas	•	29° 55 LS y 68° 05 LO</p>
            <p>Ecorregiones	•	Monte de llanuras y mesetas.</p> 
            <p>Ischigualasto se encuentra localizado a 330 Km. de la Ciudad de San Juan, en el sector Nordeste de la provincia, abarcando parte de los departamentos Jáchal y Valle Fértil. </p>
            <p>El Parque comprende 60.369 hectáreas, en su gran mayoría dispuestas en Valle Fértil y otras 2000 has. en el departamento Jáchal. Se encuentra ubicado con un centro geográfico aproximado de
              Su territorio se dispone entre un rango altitudinal de 1200 msnm en las partes más bajas de la formación ‘Ischigualasto’ y de 1900 msnm en las cumbres de las Sierras de Valle Fértil.</p>
             </div>
        
        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);
  }

  //////////////////////////////////////////////////   PARQUE NACIONAL VALLE FERTIL   ///////////////////////////////////////////////////////////////

  function climaValleFertil(){
    activarClima();
    $("#prueba").html("");
        let htmlHeader = 
        `
        <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="./src/img/Valle-Fertil/Clima-1.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Clima-2.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Clima-3.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Clima-4.png" class="imagenes d-block w-100" alt="...">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>

        `
        let htmlFin = ``
        $("#prueba").html(htmlHeader+htmlFin); 

        $("#descripcion2").html("");
        htmlHeader = 
        `
        <div class="descripcion2">

            <p>Los sistemas orográfi cos presentes en el área aíslan al sector sanjuanino de las masas de aire húmedo, imprimiéndole a este espacio un carácter netamente continental y árido.</p>
            <p>Como excepción a la aridez generalizada en el territorio sanjuanino, en el Parque Natural de Valle Fértil se encuentran áreas en forma de "lóbulos" o "islas" de menor sequedad. También es el último bastión de la extensa región semiárida argentina, en las estribaciones de la sierra de Valle Fértil al Este, con una temperatura media anual de 19,5º C. 
            El mes más cálido es Enero, con 26º C, mientras que el más frío es Julio, con 11º C.</p> 
            <p>Para esta área, los ejes de máxima precipitación media anual superan los 400 mm., con valores puntuales de 505 mm. en la Sierra de Valle Fértil. Al Sur de ésta, el máximo disminuye apreciablemente y concuerda con la magnitud de las serranías del desierto. En estos núcleos de máximos relativos se originan los ríos de régimen torrencial, los que caracterizan a los oasis ubicados alrededor del paisaje pampeano. Entre el eje descripto y otros máximos de sistemas de la provincia de La Rioja, se encuentra una extensa área llana: la cuenca sedimentaria de Los Llanos y el Gran Bajo Oriental, con menos de 250 mm. y 150 mm. respectivamente. En esta última, se destacan las Salinas de Mascasín y Pampa de las Salinas como improntas de 
            un paisaje típico del desierto.</p>
            <p>El eje de mínima para este dominio árido parte del límite de La Rioja con San Juan, y sigue hacia el Sur con valores de precipitación que - en la travesía de Mogna - oscilan alrededor de 75 mm. hasta encontrarse con la cuña orográfi ca que representa la Sierra de Pie de Palo. Esta bifurca a dicho eje en dos sectores: el Oriental, caracterizado por la travesía de Ampacama (que continúa en los Grandes Médanos del Bermejo con precipitaciones inferiores a 100 mm); y el Occidental, que penetra por el Semibolsón de Tulum (dónde se encuentra el principal asentamiento humano de la provincia de San Juan junto al oasis generado por el Río San Juan, el que fi naliza en el Norte de Mendoza). En la ladera oriental de la Sierra de Pie de Palo, se 
            observa un máximo de 200 mm. que se extiende hasta el Sur de la misma, fuera del dominio serrano.</p>
             </div>
        
        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);


        
  }

  function faunaValleFertil(){
    activarFauna();
    $("#prueba").html("");
        let htmlHeader = 
        `
        <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="./src/img/Valle-Fertil/Fauna-1.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Fauna-2.png" class="imagenes d-block w-100" alt="hola">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Fauna-3.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Fauna-4.png" class="imagenes d-block w-100" alt="hola">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Fauna-5.png" class="imagenes d-block w-100" alt="...">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>

        `
        let htmlFin = ``
        $("#prueba").html(htmlHeader+htmlFin); 

        






        $("#descripcion2").html("");
        htmlHeader = 
        `
        <div class="descripcion2">
            <p>Esta zona es una región de gran interés biogeográfi co por constituir un ecotono; es decir, una transición entre dos regiones. En este caso, representa el pasaje entre el Monte (propio de San Juan) y el Chaco Serrano y Árido. Es uno de los lugares con mayor riqueza natural de la provincia, albergando a muchas especies que sólo se hallan allí, como es el caso del Rey Del Bosque (Pheucticus aureoventris), especie sobre la que el hombre ejerce una fuerte presión de caza. </p>
            <p>El Parque Natural Valle Fértil presenta un atractivo especial por la diversidad de aves canoras que posee. Por tal motivo ha sido designada como áreas de importancia por la Comisión de las Aves (AICAs), una iniciativa de conservación a nivel mundial impulsada por las organizaciones Birdlife Internacional y Aves Argentinas. </p> 
            <p>Dentro del Parque podemos encontrar innumerables ejemplos de la adaptación de las aves a diferentes ambientes naturales. Entre las representantes del Monte encontramos al Atajacaminos Tijereta (Hydropsalis torquata), al Picafl or Cola De Cometa (Sappho sparganura) y a la Martineta Común(Eudromia elegans). En las quebradas podemos hallar a las Golondrinas Barranqueras (Notiochelidon cianoleuca), ágiles voladoras que anidan en barrancos. 
            En los ambientes de ríos, arroyos y espejos de agua, se pueden apreciar la Garza Blanca (Egretta alba), la Garcita Blanca (Egretta thula), la Garcita Azulada (Butorides striatus), la Garza Bruja (Nycticorax nycticorax), el Biguá (Phalacocorax brasilianus), el Pato Picazo (Netta peposaca), y el Pato Zambullidor Chico ( Oxyura vittata), entre otros. 
            En los ambientes chaqueños están el Chinchero Grande (Drymornis bridgesii), con su pico curvo especial para capturar pequeños insectos, o el Carpintero Del Cardón (Melanerpes cactorum) con su pico cónico diseñado para perforar agujeros en los árboles y así alimentarse de las larvas de insectos. 
            Allí también están la Chuña Patas Negras (Chunga burmeisteri) y la Bandurrita Chaqueña (Upucerthia certhiodes). Varias especies se han adaptado a ambientes antrópicos, como localidades y ámbitos rurales. Algunos ejemplos de ellas son el Chimango (Milvago chimango), la Paloma Manchada (Columba maculosa), la Torcacita (Columbina Picui), la Pirincha o Urraca (Guira guira), la Lechuza De Campanario (Tyto alba), el Picabuey (Machetornis rixosus) y el Sururi Real (Tyrannus melancholicus). 
            Hay otras especies con una distribución más amplia, como el Jote Cabeza Negra (Coragyps atratus), el Jote Real (Sarcoramphus papa), y el Águila Mora (Geranoaetus melanoleucus).</p>
            <p>Entre las especies de boas características encontramos a la Lampalagua (Boa constrictor occidentalis), que se alimenta de los roedores de la zona y también de aves. No es una especie venenosa: mata por constricción. Puede medir hasta 3 metros de largo y pesa hasta 15 Kg. 
            Es frecuente ver ejemplares de ellas cruzando los caminos del Parque Natural.</p>
            <p>Los mamíferos son el grupo de animales más evolucionado, siendo su comportamiento una fuente de observación para el hombre, quien se siente atraído por su presencia. Esto es motivo de persecuciones a través de la caza ilegal, la que llega al punto de poner en peligro su existencia en el medio natural. 
            Muchas poblaciones de distintas especies tienen serios problemas de conservación, e incluso algunas se extinguieron debido a la caza furtiva. No obstante, es posible observar al Guanaco (Lama guarnicoe) y al Puma (Puma concolor). 
            Tambien se observan pequeños mamíferos como la Comadreja Común (Didelphis albiventris), la Comadrejita Común (Thylamis pallidior), el Quirquincho Bola (Tolypeutes matacus), el Zorro Gris
            (Licalopex gymnocercus), el Zorrín Chico (Conepatus castaneus), el Hurón Mediano o Común (Galictis cuja), el Pericote Común (Graomys griseofl avus), la Vizcacha o Chinchillón (Lagidium viscacia), el Cuis Chico (Microcavia australis), el Conejo De Los Palos (Pediolagus salinicola), y la Mara (Dolichotis patagonum). Hay incluso una especie exótica introducida, como es el caso de la Liebre Europea (Lepus europaeus). </p>
            </div>

        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);

}
  
  function floraValleFertil(){
    activarFlora();
    $("#prueba").html("");
        let htmlHeader = 
        `
        <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="./src/img/Valle-Fertil/Flora-1.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Flora-2.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Flora-3.png" class="imagenes d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="./src/img/Valle-Fertil/Flora-4.png" class="imagenes d-block w-100" alt="...">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>

        `
        let htmlFin = ``
        $("#prueba").html(htmlHeader+htmlFin); 


        $("#descripcion2").html("");


        htmlHeader = 
        `
        <div class="descripcion2">
            <p>En el Parque Natural Valle Fértil se encuentra representada la provincia fi togeográfi ca del Monte, en la depresión del Río Bermejo. 
            También está representado el Chaco, con el distrito del Chaco Árido, en los llanos del departamento de Valle Fértil. El Chaco Serrano se halla en la ladera oriental de la Sierra de Valle Fértil y de la Huerta. 
            La provincia del Cardonal se encuentra en algunas quebradas de las sierras antes mencionadas, con especies cactáceas tales como Trichocereus terschesky, Trichorereus huascha, Trichocereus candicans y Cercus aethiops, entre otros.
            </p>
            <p>En algunas partes de esta área es posible observar ejemplares de Quebracho Blanco (Aspidosperma), Algarrobos (Prosopis alba y P. chilensis), Mistol (Ziziphus mistol) y Tala (Celtis tala), entre otros. 
            En algunas quebradas orientales de la Sierra de Valle Fértil y de La Huerta, se pueden encontrar bosques de Quebracho Colorado u Orco-Quebracho (Schinopsis haenkeana) junto a grupos de Molle (Lithrea molleoides) y Ancoche (Vallesia glabra), especies características del Distrito del Chaco Serrano.
            Entre las herbáceas son comunes el Limoncillo (Pectis odorata), el Chil-Chil (Tagetes minuta), el Incayuyo (Lippia integrifolia) y el Poleo (L. turbinata).</p>
          </div>
        
        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);


  }
  function geoValleFertil(){
    activarGeo();
    $("#prueba").html("");
        let htmlHeader = 
        `
        <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d777585.8986804273!2d-68.12081232839145!3d-31.292226356999794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9681f433bfc8e411%3A0xbf088efded1b4c34!2sReserva%20de%20uso%20m%C3%BAltiple%20Valle%20F%C3%A9rtil!5e0!3m2!1ses!2sar!4v1642420280140!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

        `
        let htmlFin = ``
        $("#prueba").html(htmlHeader+htmlFin); 

        $("#descripcion2").html("");
        htmlHeader = 
        `
        <div class="descripcion2">
            <p>Coordenadas	•	30° 30' S y 67° 26 O</p>
            <p>Ecorregiones	•	Monte de llanuras y mesetas.</p> 
            <p>El Parque Natural Valle Fértil se encuentra ubicado en el Centro - Este de la provincia, abarcando parte de los departamentos Caucete, Angaco, Jáchal y Valle Fértil. 
            Este espacio protegido incluye partes del valle de Ampacama, de las sierras de Pie de Palo, de las sierras de Valle Fértil - de la Huerta y del pie de monte oriental de esta última. </p>
            <p>La superficie del Parque está determinada por los siguientes límites: Al Sur, con Ruta Nacional Nº 20, partiendo desde el meridiano 68 (altura Vallecito) hasta Marayes. 
            Al Este, desde la confluencia de la Ruta Nacional Nº 20 con la Ruta Provincial Nº 293, la que pasa por San Agustín (desarrollo 80 Km. aproximadamente) hasta Usno. 
            Al Norte, desde la Ruta Provincial Nº 293, altura de Usno, y la intersección del paralelo 30º 30’ con el meridiano 68º, siguiendo por este último hasta el encuentro con la Ruta Nacional Nª 20 (altura Vallecito). 
            Superficie aproximada del polígono: 800.000 hectáreas.</p> 
            <p>ACCESOS VIALES: </p> 
            <p>Desde la Ciudad de San Juan por Ruta Nacional Nº 141 hasta la localidad de Marayes, y desde esa localidad por Ruta Prov. Nº 293 hasta la villa cabecera de San Agustín de Valle Fértil.</p> 
            <p>Desde la provincia de La Rioja: Por Ruta Nac. Nº 38 hasta Patquía. 
            Desde Patquía al Valle de la Luna, por Ruta Prov. Nº 150; y desde el Valle de la Luna hasta la Villa San Agustín, por Ruta Prov. Nº 293. </p>
            </div>
        
        `
        htmlFin = `` 
        $("#descripcion2").html(htmlHeader+htmlFin);
  }

    ////////////////////////////////////////////////////////   LA CIENAGA   ///////////////////////////////////////////////////////////////

    function climaLaCienaga(){
      activarClima();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="./src/img/Cienaga/Clima-1.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Cienaga/Clima-2.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Cienaga/Clima-3.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Cienaga/Clima-4.png" class="imagenes d-block w-100" alt="...">
          </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
          $("#descripcion2").html("");
          htmlHeader = 
          `
          <div class="descripcion2">
  
              <p>El clima de la región es de tipo semiárido. 
              Los diferentes elementos que definen las características climáticas así lo manifiestan. 
              Los datos analizados han sido registrados en la estación meteorológica que el Servicio Meteorológico Nacional (SMN) mantiene en ciudad de Jáchal. La temperatura media anual es de 16,71°C, y los valores máximos y mínimos absolutos observados son de 41,51ºC y 8,81ºC respectivamente. 
              El promedio anual de precipitación en Jáchal es de 107,1 mm., las que ocurren principalmente en verano y en forma de tormentas no frontales. </p>
              </div>
          
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
  
  
          
    }
  
    function faunaLaCienaga(){
      activarFauna();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="./src/img/Cienaga/Fauna-1.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Cienaga/Fauna-2.png" class="imagenes d-block w-100" alt="hola">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Cienaga/Fauna-3.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Cienaga/Fauna-4.png" class="imagenes d-block w-100" alt="hola">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Cienaga/Fauna-5.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Cienaga/Fauna-6.png" class="imagenes d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
          
  
  
  
  
  
  
          $("#descripcion2").html("");
          htmlHeader = 
          `
          <div class="descripcion2">
              <p>La gran variedad de especies vegetales - junto al encuentro de ambientes terrestres y acuáticos - posibilita que las aves cuenten con una variada oferta alimenticia, lo que las convierte en el grupo más diverso y representativo de La Ciénaga. 
              Uno de los grupos más llamativos por sus coloridos y ágiles movimientos son los picafl ores, que utilizan las fl ores de la Liga (Ligaria cuneifolia) como fuente de alimento. 
              En este grupo hallamos al Picafl or Común (Chlorostilbon aureoventris), al Picafl or Cola De Cometa (Sappho sparganura) y al Picafl or Andino (Oreotrochilus estella). </p>
              <p>Gracias a sus alas anchas y fuertes, los jotes planean en corrientes térmicas y agudizan sus sentidos para encontrar sus presas. Así, el Jote Cabeza Colorada (Cathartes aura) es una de las aves con mejor olfato para la detección de carroña. El Jote Cabeza Negra (Coragyps atratus) prioriza la vista para la detección de animales muertos. 
              También es posible encontrar al Chimango o Ibiña (Milvago chimango), al Halconcito Colorado (Falco sparverius) y al Halcón Peregrino (Falco peregrinus).</p> 
              <pLa Golondrina Negra (Progne modesta) y la Barranquera (Notiochelidon cyanoleuca) se ven revoloteando en el cielo mientras realizan movimientos ligeros para cazar insectos al vuelo.</p>
              <p>Entre las arboledas y arbustales hay Zorzales (Turdus chiguanco) y Loicas (Sturnella loica) alimentándose de diversos frutos de la zona. Al Pitojuan (Pitangus sulphuratus) y al Picabuey (Machetornix rixosus) se los observa mientras buscan insectos entre los cultivos y el ganado. 
              Otra especie insectívora es el Carpintero (Colaptes melanolimus) que, con su pico recto y fuerte, extrae larvas e insectos de los árboles.</p>
              <p>Cerca de los cursos de agua se encuentran las Garzas (Egretta thula, Egretta alba, Ardea cocoi, Nycticorax nycticorax), las que se especializan en la caza de reptiles y peces aprovechando su pico como un arpón o una pinza. El Biguá (Phalacrocorax olivasceus) posee un gancho en la punta del pico para evitar que el pez resbale. 
              Si bien las aves conforman el grupo más diverso en el área, también es común observar a varios mamíferos. Entre éstos se destacan el Zorro Gris (Lycalopex gymnocercus), las Comadrejas (Didelphis albiventris), los Cuis (Microcavia australis) y los Tuco Tuco (Ctenomys sp.), los cuales se alimentan preferentemente de semillas y frutos secos. Entre los reptiles encontramos lagartos del género Liolaemus y también serpientes, que aprovechan las horas de la siesta para asolearse y realizar sus actividades. 
              El llamativo colorido de la Serpiente De Coral (Micrurus pyrrhocryptus) nos anticipa su peligrosidad, aunque también las Yararás (Bothrops neuwiedi diporus y B. ammodytoides) son muy venenosas. 
              La tonalidad opaca de la piel de las Yararás les ayuda a pasar desapercibidas.</p>
              </div>
  
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
  
  }
    
    function floraLaCienaga(){
      activarFlora();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="./src/img/Cienaga/Flora-1.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Cienaga/Flora-2.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Cienaga/Flora-3.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="./src/img/Cienaga/Flora-4.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
          <img src="./src/img/Cienaga/Flora-5.png" class="imagenes d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
          <img src="./src/img/Cienaga/Flora-6.png" class="imagenes d-block w-100" alt="...">
          </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
  
          $("#descripcion2").html("");
  
  
          htmlHeader = 
          `
          <div class="descripcion2">
              <p>No sólo la policromía de sus cerros hacen de La Ciénaga un paisaje inigualable: la gran diversidad del área es producto del pleno desarrollo de la vegetación típica del Monte, la que se matiza con algunos elementos de la Prepuna. 
              Los dos endemismos de La Ciénaga están representados por una cactácea (Lobivia famatinensis var. sanjuanensis) y una verbenácea (Acantholippia trifi da). La presencia de estas especies, de restringida distribución, evidencian el excelente estado de conservación de la Reserva.
              </p>
              <p>El Río Huaco, de caudal regular, atraviesa el área de Oeste a Este. A pesar de tener un alto contenido salobre, permite el desarrollo de un ambiente húmedo propicio para el crecimiento de Totora y grandes Cortaderas.</p>
              <P>En las zonas más áridas se desarrollan extensas comunidades de añejos Algarrobos (Prosopis sp.), además de Tuscas (Acacia sp.), Retamos (Bulnesia retama), Jarillas (Larrea sp.), Tamarindos (Tamarix), Zampas (Atriplex sp.), Palo Azul (Cyclolepis genistioides) y Chañares (Geoff roea decorticans). Muchas de estas especies están parasitadas por la Liga (Ligaria cuneifolia), una hemiparásita que utiliza de soporte a los ejemplares en los que se posa extrayéndole agua y sales. La vegetación nativa de La Ciénaga se mezcla con el paisaje que aportan las actividades antrópicas, como en el caso de las zonas cultivadas.</p>
              </div>
          
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
  
  
    }
    
    function geoLaCienaga(){
      activarGeo();
      $("#prueba").html("");
          let htmlHeader =  `
          <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10953.32070305493!2d-68.58107127992297!3d-30.14913086015467!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9683404c0105b065%3A0xaee8e235e041b3ba!2sLa%20Ci%C3%A9naga%2C%20San%20Juan!5e0!3m2!1ses!2sar!4v1646316302209!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
          $("#descripcion2").html("");
          htmlHeader = 
          `
          <div class="descripcion2">
          <p>Coordenadas	•	30° 08' S y 68° 34' O</p>
          <p>Ecorregiones	•	Monte</p> 
          <p>Perteneciente al departamento Jáchal, a 25 km de su villa cabecera, La Ciénaga está ubicada en la Quebrada del Río Huaco, cuyas aguas atraviesan la reserva de Oeste a Este. La zona se encuentra inserta en un marco de sierras de mediana altura que corresponden - geológicamente - a la Precordillera Central. 
          Al Oeste, limita con el Dique Los Cauquenes y Cerro El Perico; al Este, está al límite de la Cuesta Colorada - Represa de Huaco; al Norte, el Alto Las Azucenas; y al Sur, limita con las Torres del Portezuelo o los Llanos Amarillos.</p>
          <p>ACCESOS </p> 
          <p>Desde la ciudad de San Juan se accede por la Ruta Nacional Nº 491. Saliendo de la villa cabecera (San José de Jáchal), al Noreste, y pasando la localidad de Pampa Vieja, el viajero se encuentra con un gran espejo de agua: el Dique Los Cauquenes. 
          Luego, se accede por un camino de cornisa zigzagueante y angosto (La Cuesta) que alcanza los mil metros de altura sobre el nivel del mar. Al pasar el túnel, se observan las primeras casas de los pobladores de La Ciénaga. La localidad conecta a San José de Jáchal con Huaco.</p> 
          </div>
          
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
    }

    ////////////////////////////////////////////////////////   DON CARMELO   ///////////////////////////////////////////////////////////////

    function climaDonCarmelo(){
      activarClima();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="./src/img/Don-Carmelo/Clima-1.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Clima-2.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Clima-3.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Clima-4.png" class="imagenes d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
          $("#descripcion2").html("");
          htmlHeader = 
          `
          <div class="descripcion2">
  
              <p>El clima predominante en la zona de las Lagunas de Huanacache es del tipo árido, acentuado por características desérticas y con bajos valores pluviales medios anuales. Hay algo más de un 50% de humedad relativa y una temperatura media anual que se encuentra en torno a los 18º C. Los vientos predominantes en el sector Sur de la provincia pertenecen al cuadrante Sur-Sureste.
               No obstante, cabe destacar la presencia frecuente del viento Zonda y de un viento arenoso bastante común en la zona del Encón, proveniente del cuadrante Este.</p>
              <p>La ecorregión puneña - dónde se encuentra Don Carmelo - se caracteriza por poseer défi cit de agua, irregularidad en las precipitaciones, baja humedad atmosférica, elevada heliofanía y grandes amplitudes térmicas. Estos son factores ambientales limitantes que vuelven difi cultosa la supervivencia en esta zona.
              </p>
              <p>Se registran precipitaciones escazas: menos de 100 mm al año. El clima en general es frío, con temperaturas nocturnas bajo cero la mayor parte del año y nevadas eventuales en invierno, a partir del mes de junio.</p>
              <p>"De acuerdo con el modelo climático de Köeppen (1931), el clima del área es del subtipo BWwkb, desértico hiperárido, siendo frío (temperaturas nocturnas bajo cero la mayor parte del año) y seco, con lluvias concentradas en el período estival y nevadas en invierno (Cabrera, 1976). En el año 2016 (único con registros) precipitaron 134 mm, concentrados principalmente entre diciembre y febrero cuando ocurre el 77% de las lluvias (Fig. 1). Las amplitudes térmicas diarias son superiores a los 20 °C, las
              temperaturas medias no superan los 10 °C, con mínimas absolutas de hasta -13 °C y las máximas absolutas alcanzan los 35 °C. Las nevadas se producen desde mayo a octubre (datos de estación meteorológica ubicada en valle de La Invernada)."
              </p>
              <p>Ripoll, Y. & E. Martínez Carretero. 2019. Vegetación del Valle La Invernada (Reserva Privada Don Carmelo) en el centro oeste de la Provincia de San Juan (Argentina). Bol. Soc. Argent. Bot. 53: 405-419.
              </p>
               </div>
          
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
  
  
          
    }
    function faunaDonCarmelo(){
      activarFauna();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="./src/img/Don-Carmelo/Fauna-1.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Fauna-2.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Fauna-3.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Fauna-4.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Fauna-5.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Fauna-6.png" class="imagenes d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
          
  
  
  
  
  
  
          $("#descripcion2").html("");
          htmlHeader = 
          `
          <div class="descripcion2">
              <p>En el lugar se pueden identifi car aves como el Cóndor Andino (Vultur gryphus) y otras rapaces como el Águila Mora (Geranoaetus melanoleucus), el Aguilucho Común (Buteo polyosoma) y el amenazado Halcón Peregrino (Falco peregrinus). Siguiendo con las aves, también es posible observar ejemplares de Chorlo Cabezón (Oreopholus rufcollins), Palomita dorada (Metriopelia aymara), Picafl or Gigante (Patagona gigas), Bandurrita Común (Upucerthia dumetaria), Jilguero Grande (Sicalis auriventris), Jilguero Plomizo (Phrygilus unicolor), Jilguero Chico (Phrygilus plebejus), Comesebo Andino (Phrygilus gayi), etc. 
              Dentro del grupo de mamíferos, se encuentran el Zorro (Lycalopex gymnocercus), el Puma (Puma concolor), la Vizcacha de las Sierras o Chinchillón (Lagidium viscacia), el Tuco-tuco (Ctenomys sp.) y una especie exótica introducida, como lo es la Liebre Europea (Lepus europaeus).</p>
              <p>Gracias a la protección que ha desarrollado la Provincia, en la región se pueden ver varias familias de guanacos. El Guanaco (Lama guanicoe) es el camélido silvestre americano de mayor tamaño corporal. Esta especie fue protegida por normativa provincial para así prohibir su caza. 
              Sin embargo, aún es presa de cazadores furtivos que utilizan tanto su pelaje y cuero como su carne para comercializar.</p> 
              <p> Entre los reptiles del lugar se encuentran la Lagartija (Liolaemus ruibali), el Lagarto (Pristidactylus scapulatus) y el Cola de Piche (Phymaturus palluma). También hay anfi bios como el Sapo andino (Rhinella spinulosus) que, a pesar de tener una amplia distribución en Argentina y ser muy común, es una especie poco estudiada.</p>
              
              </div>
  
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
  
    }
    function floraDonCarmelo(){
      activarFlora();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="./src/img/Don-Carmelo/Flora-1.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Flora-2.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Flora-3.png" class="imagenes d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="./src/img/Don-Carmelo/Flora-4.png" class="imagenes d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
  
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
  
          $("#descripcion2").html("");
  
  
          htmlHeader = 
          `
          <div class="descripcion2">
              <p>El área está ubicada dentro del Dominio Andino - Patagónico de la Región Neotropical. Incluye ambientes de las ecorregiones Altoandina, Puneña y - en forma menor de la Provincia del Monte - Dominio Chaqueño.</p>
              <p>El valle que corre entre las Sierras del Tigre y de la Invernada es una unidad que gana altura desde el Norte, dónde domina el Monte, hacia el Sur, donde ya en la entrada de la estancia dominan las comunidades andino - patagónicas. 
              El ecotono o transición en la región, en la vegetación, estaría ubicado entre los 2400 y 3000 m.s.n.m. La fi sonomía dominante es la de un matorral abierto que decrece en altura donde domina el pastizal de coirones (Stipa sp.), acompañada además por otros tipos de estepa como lo son la herbácea , 
              la halófi la (adaptada a suelos salinos) y la sammófi la (adaptada a sustratos arenosos).</p>
              <p>Entre los elementos del monte se encuentra presente la Jarilla (Larrea sp.). Otras especies citadas para el área son: Yerba Loca (Astragalus sp.), Leña Piedra (Azorella trifurcata), Coironcito (Carex maritima), Raíz de teñir (Galium richardianum), Margarita (Glandularia aff . microphylla), Tomillo Macho (Junellia seriphioides), Tomillo Espinoso (Junellia erinacea), 
              Cardón Blanco (Lobivia formosa), Vinagrillo (Oxalis pycnophylla), Achicoria Amarga (Taraxacum offi cinale), Violeta de Montaña (Viola montagnei), Granadilla (Mutisia sp.), el Quillay (Polygala sp.), Cebadilla Chilena (Melica chilensis), Malvavisco Puneño (Tarasa antofagastana), la Tola (Fabiana denudata), Callampa de Burro (Nasthanthus aglomeratus), Pedorrilla (Chuquiraga ruscifolia), Hierba Santa o Yerba del Parto (Pachylaena atriplicifoli), el Quinchamalí (Baccharis grisebachii) y 
              la Tramontana (Ephedra breana). También encontramos pequeñas zonas en las que las condiciones contrastan completamente con el resto del terreno. Estas áreas son las denominadas "vegas", pequeños oasis dónde el suelo se encuentra completamente saturado de agua y la vegetación se halla constituida por especies pigmeas, generalmente rimatozas, que forman un denso tapiz.</p>
              
              </div>
          
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
  
  
    
        }
    
    function geoDonCarmelo(){
      activarGeo();
      $("#prueba").html("");
          let htmlHeader = 
          `
          <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d491387.8164584714!2d-69.34148949623143!3d-30.994723859470124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x968472bd380b504b%3A0xb7efc26c3f451770!2sReserva%20Don%20Carmelo!5e0!3m2!1ses!2sar!4v1642430207624!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        
          `
          let htmlFin = ``
          $("#prueba").html(htmlHeader+htmlFin); 
  
          $("#descripcion2").html("");
          htmlHeader = 
          `
          <div class="descripcion2">
          <p>Coordenadas	•	31° 18' S y 68° 30 O</p>
          <p>Ecorregiones	•	Puna</p> 
          <p>La Reserva Privada de Uso Múltiple Don Carmelo está ubicada en el Valle de la Invernada, en el Centro - Oeste del departamento Ullum; a los 31°, 18’, 40’’ latitud Sur; y a 68°, 30’, 40’’ longitud Oeste; a una altura aproximada de 3.000 m.s.n.m. Se encuentra enclavada en la precordillera de la Provincia de San Juan, a 130 Km. aproximadamente de la Capital de San Juan.</p> 
          <p>Los límites de la Reserva son: al Sur, el Río San Juan; al Oeste, las Sierras del Tigre; al Este, las Sierras de la Invernada (que le dan nombre al valle). El Norte está constituido por una línea imaginaria que corre de Este a Oeste a la altura del Cerro Morterito.</p> 
          <p>ACCESOS:</p>
          <p>A la Estancia Don Carmelo se accede por Ruta Nacional Nº 40 hasta Talacasto. Desde allí se toma la Ruta Provincial Nº 436, hacia Iglesia y hasta la altura del puesto de Vialidad llamado “La Ciénaga” ( aprox. a 107 km de la ciudad de San Juan). Luego, se transita por una huella de unos 34 Km. que lleva a la Reserva (sólo para vehículos 4 x 4). Un poco más adelante, pasando la mina de Gualilán, se encuentra otra huella, de unos 36 Km., para todo tipo de vehículos.</p>
          
            </div>
          
          `
          htmlFin = `` 
          $("#descripcion2").html(htmlHeader+htmlFin);
    } 
  
        /////////////////////////////////////////////////////////   Paisaje Protegido Pedernal   /////////////////////////////////////////////

        function climaPedernal(){
          activarClima();
                  $("#prueba").html("");
                      let htmlHeader = 
                      `
                      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                          <img src="./src/img/Pedernal/Clima-1.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Clima-2.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Clima-3.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Clima-4.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Clima-5.png" class="imagenes d-block w-100" alt="...">
                          </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </button>
                      </div>
              
                      `
                      let htmlFin = ``
                      $("#prueba").html(htmlHeader+htmlFin); 
              
                      $("#descripcion2").html("");
                      htmlHeader = 
                      `
                      <div class="descripcion2">
              
                          <p>Ubicado a 1.340 metros de altura sobre el nivel del mar, presenta veranos templados con noches muy frescas. La temperatura media anual en la zona baja es de 18º C; la media mínima del mes de julio de 6º C; la media máxima del mes de enero es de 20,7º C. La precipitación media anual varía entre los 100 mm. al Este y los 370 mm. al Oeste. La isohieta de 100 mm. se encuentra próxima a la localidad de Pedernal; la de 200 mm. está próxima a la estancia El Durazno; la parte Este del Acequión supera los 300 mm. en la zona de la estancia Nikes.</p>
                          <p>Minetti (1986) indicó este gradiente pluvial al estimar las precipitaciones medias anuales para las estaciones de Acequión (186,5 mm), Pedernal (186,9 mm) y Nikes (269,4 mm)
                          </p>
                          
                           </div>
                      
                      `
                      htmlFin = `` 
                      $("#descripcion2").html(htmlHeader+htmlFin);
              
              
                      
        }
        function faunaPedernal(){
          activarFauna();
                  $("#prueba").html("");
                      let htmlHeader = 
                      `
                      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="./src/img/Pedernal/Fauna-1.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Fauna-2.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Fauna-3.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Fauna-4.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Fauna-5.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Fauna-6.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Fauna-7.png" class="imagenes d-block w-100" alt="...">
                          </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </button>
                      </div>
              
                      `
                      let htmlFin = ``
                      $("#prueba").html(htmlHeader+htmlFin); 
              
                      
              
              
              
              
              
              
                      $("#descripcion2").html("");
                      htmlHeader = 
                      `
                      <div class="descripcion2">
                          <p>La fauna autóctona corresponde a la del Monte precordillerano, con algunas de estas especies adaptadas a la actividad humana, pudiendose encontrar los siguientes grupos:</p>
                          <p>• Mamíferos</p> 
                          <p> El grupo de los mamíferos está en parte conformado por el Zorro Gris (Lycalopex gymnocercus) y el Zorro Colorado (Lycalopex culpaeus), los que se alimentan de pequeños roedores, carroña y algunos frutos que encuentran. También están el Chinchillon de la Sierra (Lagidium viscacia), típico habitante de los roquedales, la Mara o Liebre Patagónica (Dolichotis patagonum) y la Liebre Europea (Lepus europaeus) vista en zonas más rurales. </p>
                          <p>El Cuis (Microcavia australis), el Gato del Pajonal (Lynchailurus pajeros), la Comadreja Overa (Delphis albiventris), la Mulita (Chaetophractus sp.) y el Puma (Puma concolor) completan la lista.</p>
                          <p>• Aves (ambientes rurales)</p>
                          <p>Entre las aves podemos observar al Choique o Suri (Pterocnemia pennata), un muy buen corredor que se suele ver en grupos, y a la Martineta Común (Eudromia elegans), la que a veces sale entre los arbustos con vuelos cortos espantados o simplemente corriendo. Un ave asociada a los cursos de agua es la Garcita Blanca (Egretta thula), que se alimenta de pequeños peces insectos y anfi bios. Si observamos entre las sierras o en lo alto del cielo, podemos llegar a ver al Cóndor Andino (Vultur gryphus), al Jote Cabeza Colorada (Cathartes aura), y a algunas rapaces como el Águila Mora (Geranoeatus melanoleucus), el Milano Blanco (Eleanus lucurus), el Cernicalo o Halconcito Colorado (Falco Sparverius) y el Ibiña o Chimango (Milvago chimango). En parrales y cultivos vemos a la Lechucita Vizcachera (Athene cunicularia), al Vencejo Blanco (Aeronautas Andecolus) y al
                           Carpintero Real Común (Colapses malanolaimus), este último trabajando en los troncos de árboles en busca de insectos.</p>
                          <p>Otras aves muy comunes son el Hornero (Furnarius rufus), la Paloma Manchada (Columba maculosa), la Torcaza (Zenaida auriculata), la Torcacita Común (Columbina picuí), el Pitojuan (Pitangus sulphuratus), la Calandria Real (Mimus triurus), el Zorzal Chiguango (Turdus chiguango), el Gorrión (Passer domesticus), el Jilguero Dorado (Sicalis fl aveola), el Boquense o Comesebo Andino (Phrygilus gayi), el Chingolo (Zonotrichia capensis), el Verdón (Embernagra platenses) y el Cabecita Negra Común (Carduelos megellanica). También podemos encontrar al Tero Común (Vallenus chilensis), el que acostumbra a construir su nido en el suelo; al Corderito o Cortarramas (Phytotoma rutila), con su clasico canto parecido al sonido de un cordero; y al Benteveo (Saltador 
                            aurentiirostris), muy perseguido para su captura por ser muy buen cantor.</p>
                          
                          </div>
              
                      `
                      htmlFin = `` 
                      $("#descripcion2").html(htmlHeader+htmlFin);
              
        }
        function floraPedernal(){
          activarFlora();
                  $("#prueba").html("");
                      let htmlHeader = 
                      `
                      <div id="floraCarousel" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="./src/img/Pedernal/Flora-1.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Flora-2.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Flora-3.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Flora-4.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Flora-5.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Flora-6.png" class="imagenes d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="./src/img/Pedernal/Flora-7.png" class="imagenes d-block w-100" alt="...">
                          </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#floraCarousel" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#floraCarousel" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </button>
                      </div>
              
                      `
                      let htmlFin = ``
                      $("#prueba").html(htmlHeader+htmlFin); 
              
              
                      $("#descripcion2").html("");
              
              
                      htmlHeader = 
                      `
                      <div class="descripcion2">
                          <p>El Cordón de Pedernal - ubicado dentro de la Reserva homónima - tiene una gran importancia biogeográfi ca al constituir el limite austral Precordillerano de un conjunto de especies característica del la Ecoregion del Cardonal, las que ingresan desde el Norte del pais a la provincia de San Juan.</p>
                          <p>Se pueden distinguir importantes comunidades vegetales tales como:</p>
                          <p>• Comunidad de jarillales</p>
                          <p>Ocupa el valle y las depresiones. También domina en los interfluvios pedemontanos con un sustrato pedregoso de matriz de arena fina y limo. La Jarilla Larrea cuneifolia asciende hasta los 1.500 msnm. Entre los arbustos acompañantes, se encuentran el Jarillón Colorado (Zuccagnia punctata), el Tomillo (Acantholippia seriphioides), el Piquillín (Lycium tenuispinosum) y el Retamo (Bulnesia retama). La presencia - con valores elevados - de la Jarilla de Río (Larrea divaricata) se debe a que forma parte 
                          de la vegetación de la red de drenaje a la que está sometida la comunidad de jarillas. En el estrato herbáceo también están presentes el Pasto Amargo (Pappophorum caespitosum, P. philippianum) y la Mostacilla (Sisymbrium frutescens), entre otras. 
                          El estrato graminoso se encuentra muy afectado por el pastoreo, siendo común encontrar las especies forrajeras protegidas entre los arbustos.</p>
                          <p>• Cactáceas</p>
                          <p>Es común la presencia de suculentas. Entre ellas encontramos al Cardón (Trichocereus candicans), al Cactus Paleta (Opuntia sulphurea, Trichocereus strigossus) y a la Bola de Indio (Tephrocactus aoracantha, T. articulatus var. Oligacanthus). 
                          Comunidad de pastizales de altura. En los pisos inferiores a los 1.000 m domina el Coirón (Stipa ichu), el que suele verse favorecido por los incendios. Esta especie prácticamente desaparece a niveles superiores a los 1.900 m, llegando sólo individuos aislados hasta los 2.000 m. A partir de los 1.800 msnm. comienza a dominar la Stipa tenuissima, la que determina la fi sonomía del pastizal de altura hasta los máximos niveles montañosos (2.400 m). Estos pastizales son claramente pirógenos, siendo posible observar restos carbonizados de arbustos (fundamentalmente de Schinus fasciculata) pertenecientes a incendios reincidentes, una práctica muy común entre los lugareños. (Dalmasso, A. et al. 2002).</p>
                          <p>• Comunidad de vegetación de borde de cauce</p>
                          <p>En las riberas dominan los blanquillales de Hyalis argentea var. argentea, el Chañar Brea (Cercidium praecox, ssp.), el Molle (Schinus fasciculata), la Jarilla Melosa (Larrea nitida), el Romerillo (Eupatorium bunifolium), el Chilca Dulce (Tessaria dodonaefolia) y el Gualán (Bredemeyera colletioides), entre otras. En los márgenes de los cauces de las zonas pedemontanas hay bosquecillos de Coronilla (Colletia spinosissima). En las zonas más bajas se presentan bosques en galería de Algarrobos pertenecientes a las especies Prosopis fl exuosa y P. chilensis. Ambas especies usufructuarían el agua del subsuelo a partir de su potente sistema radical. Solamente el Algarrobo Dulce se aleja de la zona del bajo hasta los 1.200 msnm. En general, se 
                          supone que la localización de los algarrobos se encuentra limitada por razones hídricas. </p>
                          <p>• Vegetación de cauce</p>
                          <p>Existen cursos de agua permanente, como el Río Montaña, el Río del Agua y pequeñas vertientes temporarias, en las que gran parte del caudal se insume en el subálveo. En el cauce propiamente dicho, encontramos especies hidrófi tas como Llantén (Plantago australis), Mimulus glabratus, Veronica arvensis, Rorippa nasturtium aquaticum, Polypogon viridis, P. interruptus, Phylloscirpus acaulis, Ranunculus cymbalaria y Eleocharis albibracteata. En los cauces temporarios con buen drenaje, se destacan el Pájaro Bobo (Baccharis salicifolia, Baccharis retamoides), el Pasto Algodón (Digitaria californica), el Argemone subfusiformis y el Porophyllum lanceolatum.</p>
                          <p>• Vegetación de afl oramientos rocosos</p>
                          <p>Como representantes de las exposiciones más cálidas y secas, se destacan el Chaguar (Deuterocohnia longipetala), acompañada del Sanalotodo (Acantholippia aff . trifi da), el Pingo-Pingo (Ephedra multifl ora), la Yerba del Ciervo (Dolichlasium lagascae), el Arrayán de Campo (Aloysia castellanosi), la Flecha (Hyaloseris rubicunda) y la Salvialora (Budleja mendocensi).</p>
                          <p>En las laderas húmedas del Cerro Potrancas - próximas a la Estancia Nikes - aparecen bosquecillos de Piquillín (Condalia microphila) y Molle (Schinus faciculata) de gran porte, los que han sido diezmados por los frecuentes incendios producidos en el área como práctica vinculada a la actividad ganadera. Por otro lado, en el límite Este de la reserva y sobre las laderas del cordón de Pedernal, se presentan bosquecillos de Oreja de Gato (Maitenus viscyfolia), los de mayor porte registrado de la provincia.
                          El área posee un antiguo uso ganadero que se remonta a fi nales del Siglo XVI, el cual ha tenido una infl uencia directa en la introducción de especies exóticas. Para un total de 20 especies exóticas encontradas, el 35% pertenece a las compuestas. Le siguen las gramíneas (15%), mientras que el 50% restante está integrado por una especie de las siguientes familias: crucíferas, lamiáceas, rosáceas, quenopodiáceas, poligonácesas, escrofulariáceas, convolvuláceas, geraniáceas, urticáceas y portulacáceas. </p>
                          </div>
                      
                      `
                      htmlFin = `` 
                      $("#descripcion2").html(htmlHeader+htmlFin);
              
              
        }
        function geoPedernal(){
          activarGeo();
                  $("#prueba").html("");
                      let htmlHeader = 
                      `
                      <iframe class="geo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4023.925376254152!2d-68.76631270528621!3d-31.99602718077817!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9680d79135259d5d%3A0xb7d5ba70f8d2e3ec!2sPedernal%2C%20San%20Juan!5e0!3m2!1ses!2sar!4v1642771492444!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                      `
                      let htmlFin = ``
                      $("#prueba").html(htmlHeader+htmlFin); 
              
                      $("#descripcion2").html("");
                      htmlHeader = 
                      `
                      <div class="descripcion2">
                      <p>Coordenadas	•	31° 59' S y 68° 45' O</p>
                      <p>Ecorregiones	•	Altos Andes - Monte de Sierras y Bolsones</p> 
                      <p>El Valle del Pedernal se ubica al Sur de la Provincia de San Juan, en el departamento Sarmiento, cerca del límite con la Provincia de Mendoza. Yace recostado en el pedemonte de la Precordillera, a una altura aproximada de 1.240 msnm. Se encuentra en el Km. 38 de la Ruta Nacional 153 y a 107 Km. de la ciudad de San Juan. El área está inmersa en la Precordillera , delimitada al Oeste por las sierras del Tontal y al Sureste por el cordón de Pedernal.</p> 
                      <p>ACCESOS:</p> 
                      <p>Se puede acceder desde la Ciudad de San Juan por la Ruta Nacional Nº 40 hasta la localidad de Media Agua. Continuando por la Ruta Provincial Nº 153, se accede a las localidades de Cañada Honda y Los Berros hasta llegar al Dique Las Crucecitas. </p>
                      <p>Otra forma de acceso es por el departamento Calingasta. Recorriendo la Ruta Provincial Nº 412, se transpone la localidad de Barreal y el Parque Nacional El Leoncito hasta acceder a la Ruta Provincial Nº 153, que ingresa a Pedernal por el Oeste.</p>
                      <p>Desde Mendoza también se puede llegar por Ruta Nacional Nº 40 y por la Ruta Provincial Nº 39, que une Uspallata con Barreal</p>
                      
                      </div>
                      
                      `
                      htmlFin = `` 
                      $("#descripcion2").html(htmlHeader+htmlFin);
        }
        